package domain

import domain.ScheduleService.createNurseSchedule
import domain.ScheduleServiceProperties.property
import domain.SimpleTypes.*
import domain.entities.Nurse
import generators.ScheduleInfoGenerator.*
import org.scalacheck.*
import org.scalacheck.Prop.{forAll, protect}
import org.scalacheck.Test.Parameters

import scala.collection.mutable.ListBuffer
import scala.language.adhocExtensions

object TestingSeedsProperties extends Properties("ScheduleServiceProperties"):
/*
  propertyWithSeed("All Scheduling Periods have at least one Nurse Requirement", Option("4WVEQBXsgnN8r4AyqEhJv_A7OeGoOsss6uHE0JdKhvL=")) = forAll(genScheduleInfo) (  si => {
    si.schedulingPeriods.filter(sp => sp.requirements.isEmpty).size == 0
  })

  propertyWithSeed("The shift schedule must schedule all the requirements for each day", Option("8Zz_jmu5CuRYtQckNDDuEA-0FuZpdJkT4nZ_YmhklNC=")) = forAll(genScheduleInfo) (  si => {
    createNurseSchedule(si).fold( error => false, ss => ss.days.size == DayOfWeek.weekDays.size
    )
  })
//
////  ScheduleInfo(
////    List(
////      Nurse(n00001,1,List(roleA)),
////      Nurse(n00002,5,List(roleB, roleC, roleD, roleE, roleH)),
////      Nurse(n00003,2,List(roleB, roleE, roleF)),
////      Nurse(n00004,3,List(roleA))),
////    Constraint(1,7),
////    List(),
////    List(),
////    List(SchedulingPeriod(afternoon,16:00,00:00,
////      List(NurseRequirement(roleA,1),
////        NurseRequirement(roleE,1),
////        NurseRequirement(roleC,1)))))

  propertyWithSeed("The shift schedule must schedule all the requirements for each day", Option("ZWGVKFC7DiD2ZYa9WnuHHCAefmPvG7gyXkq4ulJWFkD=")) = forAll(genScheduleInfo) (  si => {
    createNurseSchedule(si).fold( error => {
      println("")
      print("Error: ")
      println(error)
      println("")
      false
    }, ss => {
      val spMap = si.schedulingPeriods.map(sp => (sp.period, sp.requirements.map(req => (req.role, req.numberOfNurses)).toMap)).toMap
      ss.days.size == DayOfWeek.weekDays.size &&
        ss.days.forall(sd => {
          val sdMap = sd.listShifts.map(s => (s.id, s.listNurses.groupBy(sn => sn.role).map(t => (t._1, t._2.size)))).toMap
          sdMap.equals(spMap)
        })
    }
    )
  })


//ScheduleInfo(
//  List(
//    Nurse(n00001,3,List(roleB, roleG, roleH, roleI, roleJ)),
//    Nurse(n00002,5,List(roleA)),
//    Nurse(n00003,5,List(roleB))
//  ),
//  Constraint(3,10),
//  List(
//    PeriodPreference(Nurse(n00001,3,List(roleB, roleG, roleH, roleI, roleJ)),-2,night),
//    PeriodPreference(Nurse(n00002,5,List(roleA)),-2,night),
//    PeriodPreference(Nurse(n00003,5,List(roleB)),-1,night),
//    PeriodPreference(Nurse(n00001,3,List(roleB, roleG, roleH, roleI, roleJ)),0,morning),
//    PeriodPreference(Nurse(n00002,5,List(roleA)),0,morning),
//    PeriodPreference(Nurse(n00003,5,List(roleB)),-1,morning),
//    PeriodPreference(Nurse(n00001,3,List(roleB, roleG, roleH, roleI, roleJ)),0,afternoon),
//    PeriodPreference(Nurse(n00002,5,List(roleA)),-2,afternoon),
//    PeriodPreference(Nurse(n00003,5,List(roleB)),1,afternoon)
//  ),
//  List(),
//  List(
//    SchedulingPeriod(night,00:00,08:00,
//      List(
//        NurseRequirement(roleB,1),
//        NurseRequirement(roleG,1),
//        NurseRequirement(roleA,1))
//      ),
//    SchedulingPeriod(morning,08:00,16:00,
//      List(
//        NurseRequirement(roleB,1),
//        NurseRequirement(roleG,1),
//        NurseRequirement(roleA,1))
//      ),
//    SchedulingPeriod(afternoon,16:00,00:00,
//      List(
//        NurseRequirement(roleB,1),
//        NurseRequirement(roleG,1),
//        NurseRequirement(roleA,1))
//    )
//))


*/
  propertyWithSeed("Test MIN_REST_DAYS Constraint", Option("2jrsTuJth0DTcHvCTJDsvGS-qDeoFzMXi3qY56up0UC=")) = forAll(genScheduleInfo) (  si => {
    createNurseSchedule(si).fold(_ => true, ss => {
      println(ss)
      val ss2 = ss
      var list = ListBuffer[Int]()

      ss.days.forall(sd => {

        sd.listShifts.forall(s => {
          s.listNurses.forall( n => {
            list.clear() //clear buffer list
            ss2.days.forall(sd2 => {
                sd2.listShifts.forall(s2 => {
                  val result = s2.listNurses.exists(n2 => {n2 == n}) //check if a nurse n exists in other shifts in other day
                  if (result == true) { //If the nurse exists add that day to the buffer list
                    list += sd2.id.to
                    println(list)
                    val workingDays = list.size //number of working days of  nurse in a ScheduleDay
                    println(si.constraint.minRestDaysWeek.to)
                    (7 - workingDays) >= si.constraint.minRestDaysWeek.to
                  } else
                    true
                })
            })
          })
        })
      })
    })
  })

  /*failing seed for ScheduleServiceProperties.The MIN_REST_DAYS Constraint for the Nurses is met when generating the ScheduleInfo is 2jrsTuJth0DTcHvCTJDsvGS-qDeoFzMXi3qY56up0UC=
    ! ScheduleServiceProperties.The MIN_REST_DAYS Constraint for the Nurses is
                                                                   met when generating the ScheduleInfo: Falsified after 0 passed tests.
  > ARG_0: ScheduleInfo(
  List(Nurse(n00001,3,List(roleB, roleC)), Nurse(n00002
    ,3,List(roleA)), Nurse(n00003,1,List(roleC, roleD, roleG, roleH)), Nurse(
    n00004,1,List(roleC, roleF, roleG, roleH)), Nurse(n00005,2,List(roleA)),
    Nurse(neA00001,5,List(roleA)), Nurse(neA00002,5,List(roleA)), Nurse(neA00
    003,1,List(roleA)), Nurse(neA00004,5,List(roleA)), Nurse(neD00001,5,List(
    roleD)), Nurse(neD00002,5,List(roleD)), Nurse(neD00003,5,List(roleD)), Nu
  rse(neD00004,1,List(roleD)), Nurse(neD00005,1,List(roleD)), Nurse(neC0000
  1,5,List(roleC)), Nurse(neC00002,1,List(roleC)), Nurse(neC00003,1,List(ro
    leC))),Constraint(5,1),List(),List(),List(SchedulingPeriod(SP01,00:00,00:
    00,List(NurseRequirement(roleA,1), NurseRequirement(roleD,1), NurseRequir
    ement(roleC,1)))))
  */
/*
  ShiftSchedule(
  List(
  ScheduleDay(1,
  List(
  Shift(SP01,List(
  ShiftNurse(Nurse(n00001,3,List(roleB, roleC)),roleC),
  ShiftNurse(Nurse(n00002,3,List(roleA)),roleA),
  ShiftNurse(Nurse(n00003,1,List(roleC, roleD, roleG, roleH)),roleD))))),

  ScheduleDay(2,List(Shift(SP01,List(
  ShiftNurse(Nurse(n00001,3,List(roleB, roleC)),roleC),
  ShiftNurse(Nurse(n00002,3,List(roleA)),roleA),
  ShiftNurse(Nurse(n00003,1,List(roleC, roleD, roleG, roleH)),roleD))))),


  ScheduleDay(3,List(Shift(SP01,List(
  ShiftNurse(Nurse(n00001,3,List(roleB, roleC)),roleC),
  ShiftNurse(Nurse(n00002,3,List(roleA)),roleA),
  ShiftNurse(Nurse(n00003,1,List(roleC, roleD, roleG, roleH)),roleD))))),

  ScheduleDay(4,List(Shift(SP01,List(
  ShiftNurse(Nurse(n00001,3,List(roleB, roleC)),roleC),
  ShiftNurse(Nurse(n00002,3,List(roleA)),roleA),
  ShiftNurse(Nurse(n00003,1,List(roleC, roleD, roleG, roleH)),roleD))))),

  ScheduleDay(5,List(Shift(SP01,List(
  ShiftNurse(Nurse(n00001,3,List(roleB, roleC)),roleC),
  ShiftNurse(Nurse(n00002,3,List(roleA)),roleA),
  ShiftNurse(Nurse(n00003,1,List(roleC, roleD, roleG, roleH)),roleD))))),

  ScheduleDay(6,List(Shift(SP01,List(
  ShiftNurse(Nurse(n00001,3,List(roleB, roleC)),roleC),
  ShiftNurse(Nurse(n00002,3,List(roleA)),roleA),
  ShiftNurse(Nurse(n00003,1,List(roleC, roleD, roleG, roleH)),roleD))))),

  ScheduleDay(7,List(Shift(SP01,List(
  ShiftNurse(Nurse(n00001,3,List(roleB, roleC)),roleC),
  ShiftNurse(Nurse(n00002,3,List(roleA)),roleA),
  ShiftNurse(Nurse(n00003,1,List(roleC, roleD, roleG, roleH)),roleD)))))))
*/