package domain

import domain.ScheduleService.createNurseSchedule
import domain.SimpleTypes.*
import domain.DomainError.*
import domain.entities.{ScheduleDay, Shift, ShiftNurse, Nurse}

import scala.language.adhocExtensions
import org.scalacheck.*
import org.scalacheck.Test.Parameters
import org.scalacheck.Prop.forAll
import generators.ScheduleInfoGenerator.*
import scala.collection.mutable.ListBuffer

object ScheduleServiceProperties extends Properties("ScheduleServiceProperties"):

  property("All Nurses have at least one Role") = forAll(genScheduleInfo) (  si => {
    si.nurses.filter(nurse => nurse.roles.isEmpty).size == 0
  })

  property("All Nurses don't repeat Nurse Roles") = forAll(genScheduleInfo) (  si => {
    si.nurses.forall(nurse => nurse.roles.size == nurse.roles.distinct.size)
  })

  property("There is at least one Scheduling Period") = forAll(genScheduleInfo) (  si => {
    si.schedulingPeriods.size > 0
  })

  property("All Scheduling Periods have at least one Nurse Requirement") = forAll(genScheduleInfo) (  si => {
    si.schedulingPeriods.filter(sp => sp.requirements.isEmpty).size == 0
  })

  property("All Scheduling Periods don't repeat Nurse Requirement") = forAll(genScheduleInfo) (  si => {
    si.schedulingPeriods.forall(sp => sp.requirements.size == sp.requirements.distinct.size)
  })

  property("All Nurse Requirement are from existing Nurse Roles") = forAll(genScheduleInfo) (  si => {
    val nurseRoles = si.nurses.map(nurse => nurse.roles).flatten.toSeq
    si.schedulingPeriods.forall(sp => sp.requirements.forall(req => nurseRoles.contains(req.role)))
  })

  property("All Day Preferences are from existing Nurses") = forAll(genScheduleInfo) (  si => {
        si.dayPreference.forall(dp => si.nurses.contains(dp.nurse))
  })

  property("All Period Preferences are from existing Nurses") = forAll(genScheduleInfo) (  si => {
    si.periodPreference.forall(pp => si.nurses.contains(pp.nurse))
  })

  property("All Period Preferences are from existing Periods") = forAll(genScheduleInfo) (  si => {
    val periods = si.schedulingPeriods.map(sp => sp.period).toSeq
    si.periodPreference.forall(pp => periods.contains(pp.periodString))
  })

  property("There are no repeated Day Preferences") = forAll(genScheduleInfo) (  si => {
    si.dayPreference.size == si.dayPreference.distinct.size
  })

  property("There are no repeated Period Preferences") = forAll(genScheduleInfo) (  si => {
    si.periodPreference.size == si.periodPreference.distinct.size
  })

  property("A nurse cannot be used in the same shift with different roles") = forAll(genScheduleInfo) (  si => {
    createNurseSchedule(si).fold(_ => true, ss => {
      ss.days.forall(sd => {
        sd.listShifts.forall(s => {
          s.listNurses.map(sn => sn.nurse).size == s.listNurses.map(sn => sn.nurse).distinct.size
        })
      })
    })
  })

  property("When the scheduling algorithm  is run several times, it should produce the same result.") = forAll(genScheduleInfo) (  si => {
    createNurseSchedule(si).fold(_ => true, ss => {
      createNurseSchedule(si).fold(_ => true, ss2 => {
        ss == ss2
      })
    })
  })

  property("The shift schedule must schedule all the requirements for each day") = forAll(genScheduleInfo) (  si => {
    createNurseSchedule(si).fold( error => {
      println("")
      print("Error: ")
      println(error)
      println("")
      false
    }, ss => {
      val spMap = si.schedulingPeriods.map(sp => (sp.period, sp.requirements.map(req => (req.role, req.numberOfNurses)).toMap)).toMap
      ss.days.size == DayOfWeek.weekDays.size &&
        ss.days.forall(sd => {
          val sdMap = sd.listShifts.map(s => (s.id, s.listNurses.groupBy(sn => sn.role).map(t => (t._1, t._2.size)))).toMap
          sdMap.equals(spMap)
        })
    }
    )
  })

  property("There cannot be a nurse duplication on a shift; each nurse must be unique.") = forAll(genScheduleInfo) (  si => {
    createNurseSchedule(si).fold(_ => true, ss => {

            //val nurseCount : Map[(DayOfWeek, NonEmptyStringST ,Nurse), Int] = ss.days.flatMap(sd => sd.listShifts.flatMap(shift => shift.listNurses.map(shiftNurse => (sd.id, shift.id, shiftNurse.nurse))))
             // .groupBy((day, period, nurse) => (day, period, nurse))
             // .map((tuple, list) => (tuple, list.size))
           // nurseCount.forall { case (_, count) => count <= 1}

      ss.days.forall(sd => {
        sd.listShifts.forall(s => {
          s.listNurses.map(sn => sn.nurse).groupBy(n => n.name).view.mapValues(_.size).filter(_._2 > 1).size==0
        })
      })
          //val lshifts = ss.days.map(d => d.listShifts).flatten
          //lshifts.forall(shift => {
          //val listShiftNurses = shift.map(s => s.listNurses).flatten
            //shift.listNurses.map(sn => sn.nurse).groupBy(n => n.name).view.mapValues(_.size).filter(_._2 > 1).size==0
         //})
      }
    )
  })


  property("The MAX_SHIFTS_PER_DAYS Constraint for the Nurses is met when generating the ScheduleInfo") = forAll(genScheduleInfo) (  si => {
    createNurseSchedule(si).fold(_ => true, ss => {
//      val nurseCount : Map[(DayOfWeek, Nurse), Int] = ss.days.flatMap(sd => sd.listShifts.flatMap(shift => shift.listNurses.map(shiftNurse => (sd.id, shift.id, shiftNurse.nurse))))
//        .groupBy((day, _, nurse) => (day, nurse))
//        .map((tuple, list) => (tuple, list.size))
//      nurseCount.forall { case (_, count) => count <= si.constraint.maxShiftsPerDay.to}
//
      val ss2 = ss
      ss.days.forall(sd => {
        sd.listShifts.forall(s => {
          s.listNurses.forall( n => {
            ss2.days.forall(sd2 => {
              if (sd2.id.to == sd.id.to) { // same day
                sd2.listShifts.forall(s2 => {
                  if (s2.id.toString.equals(s.id.toString) == false) { //different shifts
                    val result = s2.listNurses.count(n2 => {n2 == n}) //check if nurse n exists in other shifts in the same day
                    result <= si.constraint.maxShiftsPerDay.to
                  } else true
                })
              } else true
            })
          })
        })
      })
    })
  })

  property("The MIN_REST_DAYS Constraint for the Nurses is met when generating the ScheduleInfo") = forAll(genScheduleInfo) (  si => {
    createNurseSchedule(si).fold(_ => true, ss => {
      val ss2 = ss
      var list = ListBuffer[Int]()

      ss.days.forall(sd => {

        sd.listShifts.forall(s => {
          s.listNurses.forall( n => {

            list.clear() //clear buffer list

            ss2.days.forall(sd2 => {
              sd2.listShifts.forall(s2 => {
                val result = s2.listNurses.exists(n2 => {n2 == n}) //check if a nurse n exists in other shifts in other day
                if (result == true) { //If the nurse exists add that day to the buffer list
                  list += sd2.id.to

                  val workingDays = list.size //number of working days of  nurse in a ScheduleDay
                  (7 - workingDays) >= si.constraint.minRestDaysWeek.to
                } else
                  true
              })
            })
          })
        })
      })
    })
  })

