package domain
import scala.language.adhocExtensions
import org.scalatest.funsuite.AnyFunSuite
import domain.SimpleTypes.*
import domain.DomainError.*

import java.time.LocalTime
import java.time.format.DateTimeFormatter

class SimpleTypesTest extends AnyFunSuite:

  val NonEmptyString1 = "ABCD"
  val NonEmptyString2 = ""

  val Seniority0 = -1
  val Seniority1 = 1
  val Seniority2 = 5
  val Seniority3 = 3
  val Seniority4 = 10

  val Preference0 = -3
  val Preference1 = -2
  val Preference2 = 2
  val Preference3 = 1
  val Preference4 = 7

  val Number0 = -1
  val Number1 = 0
  val Number2 = 2

  val formatter = DateTimeFormatter.ofPattern("HH:mm:ss")
  val InvalidTime1 = ""
  val InvalidTime2 = "14"
  val InvalidTime3 = "14:"
  val InvalidTime4 = "14:00"
  val InvalidTime5 = "14:00:"
  val ValidTime = "14:00:01"

  val DayOfWeek0 = -3
  val DayOfWeek1 = 1
  val DayOfWeek2 = 7
  val DayOfWeek3 = 5
  val DayOfWeek4 = 10


  test("Test NonEmptyString") {
    assert(NonEmptyStringST.from(NonEmptyString1)===Right(NonEmptyString1))
    assert(NonEmptyStringST.from(NonEmptyString2)===Left(EmptyName))
  }

  test("Test Seniority") {
    assert(Seniority.from(Seniority0)===Left(SeniorityValueOutOfRange(Seniority0)))
    assert(Seniority.from(Seniority1)===Right(Seniority1))
    assert(Seniority.from(Seniority2)===Right(Seniority2))
    assert(Seniority.from(Seniority3)===Right(Seniority3))
    assert(Seniority.from(Seniority4)===Left(SeniorityValueOutOfRange(Seniority4)))
  }

  test("Test Preference Value") {
    assert(Value.from(Preference0)===Left(PreferenceValueOutOfRange(Preference0)))
    assert(Value.from(Preference1)===Right(Preference1))
    assert(Value.from(Preference2)===Right(Preference2))
    assert(Value.from(Preference3)===Right(Preference3))
    assert(Value.from(Preference4)===Left(PreferenceValueOutOfRange(Preference4)))
  }

  test("Test Number") {
    assert(Number.from(Number0)===Left(InvalidNumber(Number0)))
    assert(Number.from(Number1)===Right(Number1))
    assert(Number.from(Number2)===Right(Number2))
  }

  test("Test Period Time") {
    assert(PeriodTime.from(InvalidTime1)===Left(InvalidTimeFormat))
    assert(PeriodTime.from(InvalidTime2)===Left(InvalidTimeFormat))
    assert(PeriodTime.from(InvalidTime3)===Left(InvalidTimeFormat))
    assert(PeriodTime.from(InvalidTime4)===Left(InvalidTimeFormat))
    assert(PeriodTime.from(InvalidTime5)===Left(InvalidTimeFormat))
    assert(PeriodTime.from(ValidTime)===Right(LocalTime.parse(ValidTime, formatter)))
  }

  test("Test DayOfWeek") {
    assert(DayOfWeek.from(DayOfWeek0)===Left(InvalidDayOfWeek(DayOfWeek0)))
    assert(DayOfWeek.from(DayOfWeek1)===Right(DayOfWeek1))
    assert(DayOfWeek.from(DayOfWeek2)===Right(DayOfWeek2))
    assert(DayOfWeek.from(DayOfWeek3)===Right(DayOfWeek3))
    assert(DayOfWeek.from(DayOfWeek4)===Left(InvalidDayOfWeek(DayOfWeek4)))
  }