import domain.ScheduleServiceProperties.*
import domain.SimpleTypes.WeekDay
import generators.NurseGenerator.{MAX_ASCII_LETTER, MIN_ASCII_LETTER, MIN_NURSE_ROLES, MAX_NURSE_ROLES}
import generators.PeriodGenerator.genPeriods
import org.scalacheck.Gen

val numberOfRoles = Gen.chooseNum(MIN_NURSE_ROLES, MAX_NURSE_ROLES).sample.get
val alphabet = List.range(MIN_ASCII_LETTER, MAX_ASCII_LETTER).map(ascii => ascii.toChar)
val roleLettersList = alphabet.take(numberOfRoles)

var t = WeekDay.values.toList

val numberOfDaysOfWeek = Gen.chooseNum(1, 7).sample.get

val test = Gen.listOfN(numberOfDaysOfWeek, Gen.oneOf(WeekDay.values.toList)).sample.get

val periods = genPeriods.sample.get

val x = periods.map(p => p._1)

