import domain.SimpleTypes.{DayOfWeek, NonEmptyStringST, Number, NurseRole}
import domain.entities.{Nurse, NurseRequirement, SchedulingPeriod}
import generators.ScheduleInfoGenerator.genScheduleInfo
import domain.ScheduleServiceV2

//val si = genScheduleInfo.sample.get
//val nurses = si.nurses
//val count = si.schedulingPeriods.size
//val sp1 =  si.schedulingPeriods.head
//val sp2 =  si.schedulingPeriods(1)
//val reqs1 = si.schedulingPeriods.head.requirements
//val constraint = si.constraint
//val dayOne = DayOfWeek.fromOne

val total = 7
val t = (0 to (total - 1)).toList
val t1 = (7 / total) +1
val t2 = List.fill(7)(t).flatten
val t3 = t2.combinations(7).toList
t3.size
val expected = List(4,0,0,0,0,1,2)
val has = t3.contains(expected)

val t4 = (0 to 7).combinations(7).flatMap(_.permutations).toList
t4.size
val has1 = t4.contains(expected)

val t51 = List.fill(7)(0 to 5)
val t52 = t51.flatten
val t53 = t52.combinations(7).toList
t53.size
val t5 = List.fill(7)(0 to 4).flatten.combinations(7).flatMap(_.permutations).toList
t5.size
val has2 = t5.contains(expected)

val tsample = List(List(1,3,2), List(1,3,4), List(1,4,2), List(1,4,3), List(3,4,2))
val tSample1 = List.fill()(tsample).flatten.combinations(7).flatMap(_.permutations).toList
tSample1.size

//val t = List(List(1,3,2), List(1,3,4), List(1,4,2), List(1,4,3), List(3,4,2))
//val t2 = List.fill(7)(t).flatten
//t2.size
//val t3 = t2.combinations(7).toList
//t3.size
//t3.filter(subList=> subList.contains(List(3,4,2)))
//t3.filter(subList=> subList.head == (List(3,4,2)))
//t3.filter(subList=>subList.head == (List(3,4,2))
//  && subList.contains(List(1,3,4))
//  && subList.contains(List(1,4,2)))

//val range = (0 to 6).toList
//val rangeTest = (0 to 34).toList
//val combRangeTest = rangeTest.combinations(7).toList
//combRangeTest.size
//val list = List(1, 2, 3)
//val te = list.combinations(3).toList
//val t = List.fill(2)(list).flatten

////val allReqsCombinations = ScheduleServiceV2.generateCombinationsForAllRequirements(nurses, reqs1)
//val singlePeriodCombinations = ScheduleServiceV2.generateCombinationsForSinglePeriod(nurses, sp1)
//val singlePeriodCombinations2 = ScheduleServiceV2.generateCombinationsForSinglePeriod(nurses, sp2)
//val allPeriodsCombinations = ScheduleServiceV2.generateCombinationsForAllPeriods(nurses, si.schedulingPeriods, constraint)
//val singlePeriodCombinationsCombined = singlePeriodCombinations ++ singlePeriodCombinations2
//singlePeriodCombinationsCombined.flatMap((period, subList) => subList.map((nurse, _) => (nurse, period))).distinct.groupBy(identity)
//singlePeriodCombinationsCombined.map( shift => shift._2.map( (n,_) => n._1)).flatten
//singlePeriodCombinationsCombined.flatMap((period, subList) => subList.map((nurse, _) => (nurse, period))).distinct.groupBy((nurse, _) => nurse)
//singlePeriodCombinationsCombined.flatMap((period, subList) => subList.map((nurse, _) => (nurse, period))).distinct.groupBy((nurse, _) => nurse).map((nurse, list) => (nurse, list.size))
//  //.groupBy(identity).map(n => (n._1, n._2.size))
//
//
//val mapOne = ScheduleServiceV2.countNumberOfNursesInPeriods(singlePeriodCombinations)
//val mapTwo = ScheduleServiceV2.countNumberOfNursesInPeriods(singlePeriodCombinations2)
//
//ScheduleServiceV2.checkConstraintByValue(mapOne, mapTwo, si.constraint)
//
//






//
////val test = ScheduleServiceV2.nursesForAllRequirementsForDayAndPeriod(dayOne, sp1, nurses)
////val test2 = ScheduleServiceV2.nursesForRequirementForDayAndPeriod(dayOne, sp1.period, nurses)(sp1.requirements.head)
//
////val testDayOne = ScheduleServiceV2.nursesForAllRequirementsForDay(dayOne, si.schedulingPeriods, nurses)
////val testDayOneHead = testDayOne.getOrElse(List(List()))
////
//val testHeadSchedulingPeriodResult =  ScheduleServiceV2.nurseCombinationForSinglePeriod(nurses, List(), sp1)
////val testHeadRequirement = ScheduleServiceV2.nurseCombinationForSingleRequirement(nurses, List(), reqs1.head)
//
//val testHeadSchedulingPeriod = testHeadSchedulingPeriodResult.getOrElse(List(List()))
//
////val test : List[(Nurse, NonEmptyStringST)] = testHeadSchedulingPeriod.flatMap(list => list.map((nurse, _, period) => (nurse, period)) )
////val nonEmptyStringSTFromTest = NonEmptyStringST.fromV2("TEST")
////val testDouble  : List[(Nurse, NonEmptyStringST)] = testHeadSchedulingPeriod.flatMap(list => list.map((nurse, _, _) => (nurse, nonEmptyStringSTFromTest)) )
////val fullTest = (test ++ testDouble.take(testDouble.size/2))
////val fullTestGroup = fullTest.groupBy(_._1).view.mapValues(list => list.map((_, period) => period).distinct).view.mapValues(_.size).filter((_, occur) => occur >= 2).map((nurse, _) => nurse).toList
////val fullTestGroup1 = fullTest.groupBy(_._1).view.mapValues(_.size).toList//.filter((_, occur) => occur >= 2)
//////val test1 = fullTest.toMap
//
//testHeadSchedulingPeriod.flatten.groupBy(_._1).view.mapValues(list => list.map((_, _, period) => period).distinct).view.mapValues(_.size).filter((_, occur) => occur >= 2).map((nurse, _) => nurse).toList

//val accum : List[List[(Nurse, NurseRole, NonEmptyStringST)]] = List(List())
//val test = accum.flatMap(list=> list.map( (nurse, _, period) => (nurse, period)))
//val test1 = accum.map(list=> list.map( (nurse, _, period) => (nurse, period))).flatten.grou
//test


//val req1 = sp1.requirements.head
//val extraReq = NurseRequirement(req1.role, Number.from(req1.numberOfNurses.to * 10).getOrElse(Number.fromZero))




//def nursesWithRoles(nurses : List[Nurse], role: NurseRole) : List[Nurse] =
//  nurses.filter(nurse => nurse.roles.contains(role))
//
//def nursesForRequirement(nurses : List[Nurse], req: NurseRequirement) : List[List[(NurseRole, Nurse)]] =
//  val availNurses = nursesWithRoles(nurses, req.role).map(nurse => (req.role, nurse))
//  // if (availNurses.size >= req.numberOfNurses else ERROR
//  availNurses.combinations(req.numberOfNurses.to).toList
//
//val si = genScheduleInfo.sample.get
//val nurses = si.nurses
//val reqs1 = si.schedulingPeriods.head.requirements
//
//val t = reqs1.map(req => {
//  nursesForRequirement(nurses, req)
//})

