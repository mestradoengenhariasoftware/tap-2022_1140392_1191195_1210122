package domain

import generators.ConstraintGenerator.*
import generators.DayPreferencesGenerator.genDayPreferences
import generators.NurseGenerator.genNurses
import generators.PeriodGenerator.genPeriods
import generators.PeriodPreferencesGenerator.genPeriodPreferences
import generators.ScheduleInfoGenerator.genScheduleInfo
import generators.SchedulingPeriodGenerator.genSchedulingPeriods

import org.scalacheck.*
import org.scalacheck.Prop.forAll
import org.scalacheck.Test.Parameters

import scala.language.adhocExtensions

object GeneratorProperties extends Properties("GeneratorProperties"):
  val MIN_NURSES = 3

  property("PeriodsGenerator must generate at least 1 and no more than 10 periods")  = forAll(genPeriods) (p => {
    p.size >= 1 && p.size <= 10
  })

  property("NurseGenerator must generate at least 3 and no more than 10 nurses") = forAll(genNurses(MIN_NURSES)) (  n => {
    n.size >= 3 && n.size <= 10
  })

  property("NurseGenerator must generate nurses with seniority between 1 and 5") = forAll(genNurses(MIN_NURSES)) (  n => {
    n.forall(nurse => {nurse.seniority.to >= 1 && nurse.seniority.to <= 5})
  })

  property("NurseGenerator must generate nurses with at least 1 role and no more than 5") = forAll(genNurses(MIN_NURSES)) (  n => {
    n.forall(nurse => {nurse.roles.size >= 1 && nurse.roles.size <= 5})
  })

  property("NurseGenerator must generate nurses with roles that only have alphabetic letters") = forAll(genNurses(MIN_NURSES)) (  n => {
    n.forall(nurse => {
      nurse.roles.forall(role => {
        role.toString.toCharArray.forall( c => c.isLetter)
      })
    })
  })

  property("ConstraintGenerator must generate a constraint with at least 1 Rest Day and no more than 5") =
    val const = genConstraint
    forAll(const)( c => {
      c.minRestDaysWeek.to >= 1 && c.minRestDaysWeek.to <= 5
    })

  property("ConstraintGenerator must generate a constraint with at least 1 Max Shift Per Day and no more than 3") =
    val const = genConstraint
    forAll(const)( c => {
      c.maxShiftsPerDay.to >= 1 && c.maxShiftsPerDay.to <= 5
    })

  property("ScheduleInfoGenerator must generate at least 1 nurse requirement for period") =
    val sInfo = genScheduleInfo
    forAll(sInfo)( sInfo => {sInfo.schedulingPeriods.forall( nr => nr.requirements.size >= 1 )
    })

  property("DayPreferenceGenerator must generate preference values between -2 and 2") =
    val dayPrefList = genDayPreferences(genNurses(MIN_NURSES).sample.get)
    forAll(dayPrefList)( dPref => {dPref.forall(dp => dp.value.to >= -2 && dp.value.to <= 2)
    })

  property("PeriodPreferencesGenerator must generate preference values between -2 and 2") =
    val periods = genPeriods.sample.get
    val p = periods.map(p => p._1)
    val periodPrefList = genPeriodPreferences(genNurses(periods.size).sample.get,p)
    forAll(periodPrefList)( pPref => {pPref.forall(pp => pp.value.to >= -2 && pp.value.to <= 2)
    })