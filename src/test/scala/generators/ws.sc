import domain.ScheduleService.createNurseSchedule
import domain.SimpleTypes.{PeriodTime, *}
import domain.entities.*
import generators.ConstraintGenerator.*
import generators.ExtraNursesGenerator.*
import generators.NurseGenerator.*
import generators.DayPreferencesGenerator.*
import generators.PeriodGenerator.*
import generators.PeriodPreferencesGenerator.*
import generators.SchedulingPeriodGenerator.*
import generators.ScheduleInfoGenerator.*

import java.time.LocalTime
import java.time.format.DateTimeFormatter
import org.scalacheck.Gen

import scala.math.{max, min}

val x = List("aa", "bbb", "bb", "bbb").groupBy(identity)

val y = x.view.mapValues(_.size).filter(_._2 > 1).toMap

val si = genScheduleInfo.sample.get

val ss = createNurseSchedule(si)
val shif = ss.fold(error => Left(error), ss => Right(ss.days.map(d => d.listShifts).flatten))
var sn = shif.fold(error => Left(error), shift => Right(shift.map(s => s.listNurses).flatten))
var ln = sn.fold(error => Left(error), shiftNurse => Right(shiftNurse.map(sn => sn.nurse)))
//
//val periods = genPeriods.sample.get
//val nurses = genNurses(3).sample.get
//val constraint = genConstraint.sample.get
//val schedulingPeriods = genSchedulingPeriods(nurses, constraint, periods).sample.get
////val allNurseRequirements = schedulingPeriods.map(sp => sp.requirements.map(req => (sp.period, req.role, req.numberOfNurses))).flatten
////val allNurseRequirementsV2 = schedulingPeriods.map(sp => sp.requirements.map(req => (req.role, req.numberOfNurses))).flatten
////val rolesMap = schedulingPeriods.map(sp => sp.requirements.map(req => (req.role, req.numberOfNurses))).flatten
////  .groupBy(tuple => tuple._1).map(t => (t._1, t._2.foldLeft[(Int, Int)]((0, 0))((accum, tuple) => (accum._1 + tuple._2.to, nurses.filter(nurse => nurse.roles.contains(tuple._1)).size))))
////
////val test = List.range(0, 2)
//val extraNurses = genExtraNurses(schedulingPeriods, constraint, nurses).sample.get
//
////val periods = genPeriodsV2.sample.get
//////val numberOfPeriods = Gen.chooseNum(1, 10).sample.get
//////val startTime = LocalTime.of(0,0,0)
//////val spNumberList = List.range(0, numberOfPeriods)
//////val hoursInDay = 24 / spNumberList.size
//////val minutesInDay = (24 % spNumberList.size) / 100.0 * 60
//////val test = (24 % spNumberList.size)
////
//////val numberOfPeriods = 5
////val numberOfPeriods = Gen.chooseNum(1, 10).sample.get
////val periodPercentage = 24 / (numberOfPeriods * 1.0)
////val hoursInPeriod = math.floor(periodPercentage).toInt
////val test = (periodPercentage - hoursInPeriod) * 60.0
////val minutesInPeriod = math.round((periodPercentage - hoursInPeriod) * 60.0).toInt
////val secondsInPeriod = 0
////val baseTimeStr = "00:00:00"
////val incrementTimeStr = "%02d".format(hoursInPeriod) + ":" + "%02d".format(minutesInPeriod).toString + ":00"
////val baseTime  = PeriodTime.fromZero
////val incrementTime = PeriodTime.from(hoursInPeriod, minutesInPeriod, 0).getOrElse(null)
//////val baseTime = PeriodTime.from(baseTimeStr)
//////val incrementTime = PeriodTime.from(incrementTimeStr)
////val newTime = baseTime.add(hoursInPeriod, minutesInPeriod, 0)
////
////val idList = List.range(0, numberOfPeriods)
////
////val times = idList.map(index =>
////{
////  val startHour = hoursInPeriod * index
////  val startMinutes = minutesInPeriod * index
////  val startSeconds = secondsInPeriod * index
////  val endHour = hoursInPeriod * (index + 1)
////  val endMinutes = minutesInPeriod * (index + 1)
////  val endSeconds = secondsInPeriod * (index + 1)
////
////  (baseTime.add(startHour, startMinutes, startSeconds), baseTime.add(endHour, endMinutes, endSeconds))
////})
////
////
////
////
////
//////val hoursInPeriod = 24 / numberOfPeriods
//////val minutesInPeriod =  math.round(((24 / (numberOfPeriods * 1.0)) - hoursInPeriod) * 60)
////
////
//////
//////
//////val nurse1 =
//////  for
//////    name <- NonEmptyStringST.from("n1")
//////    seniority <- Seniority.from(5)
//////    roleA <- NurseRole.from("A")
//////  yield Nurse(name, seniority, Seq[NurseRole](roleA))
//////
//////val nurse2 =
//////  for
//////    name <- NonEmptyStringST.from("n2")
//////    seniority <- Seniority.from(4)
//////    roleB <- NurseRole.from("B")
//////  yield Nurse(name, seniority, Seq[NurseRole](roleB))
//////
//////val nurse3 =
//////  for
//////    name <- NonEmptyStringST.from("n3")
//////    seniority <- Seniority.from(3)
//////    roleA <- NurseRole.from("A")
//////    roleB <- NurseRole.from("B")
//////  yield Nurse(name, seniority, Seq[NurseRole](roleA, roleB))
//////
//////val nurse4 =
//////  for
//////    name <- NonEmptyStringST.from("n4")
//////    seniority <- Seniority.from(2)
//////    roleA <- NurseRole.from("A")
//////    roleB <- NurseRole.from("B")
//////  yield Nurse(name, seniority, Seq[NurseRole](roleA, roleB))
//////
//////val spEither =
//////  for
//////    morning <- NonEmptyStringST.from("morning")
//////    morningStartTime <- PeriodTime.from("08:00:00")
//////    morningEndTime <- PeriodTime.from("16:00:00")
//////    afternoon <- NonEmptyStringST.from("afternoon")
//////    afternoonStartTime <- PeriodTime.from("16:00:00")
//////    afternoonEndTime <- PeriodTime.from("00:00:00")
//////    n2 <- Number.from(2)
//////    n1 <- Number.from(1)
//////    roleA <- NurseRole.from("A")
//////    roleB <- NurseRole.from("B")
//////    nrA = NurseRequirement(roleA, n2)
//////    nrB = NurseRequirement(roleB, n1)
//////    spM = SchedulingPeriod(morning, morningStartTime, morningEndTime, Seq(nrA))
//////    spA = SchedulingPeriod(afternoon, afternoonStartTime, afternoonEndTime, Seq(nrB))
//////  yield List(spM, spA)
//////
//////val shifts =
//////  for
//////    morning <- NonEmptyStringST.from("morning")
//////    afternoon <- NonEmptyStringST.from("afternoon")
//////    roleA <- NurseRole.from("A")
//////    roleB <- NurseRole.from("B")
//////    n1 <- nurse1
//////    n2 <- nurse2
//////    n3 <- nurse3
//////    snM1 = ShiftNurse(n1, roleA)
//////    snM3 = ShiftNurse(n3, roleA)
//////    snM2 = ShiftNurse(n2, roleB)
//////    snA2 = ShiftNurse(n2, roleB)
////////    sM = Shift(morning, Seq(snM1, snM3, snM2))
//////    sM = Shift(morning, Seq(snM1, snM3))
//////    sA = Shift(afternoon, Seq(snA2))
//////  yield Seq(sM, sA)
//////
//////val sd = ScheduleDay(DayOfWeek.fromOne, shifts.getOrElse(Seq[Shift]()))
//////
//////val sp = spEither.getOrElse(null)
//////
//////val sdMap = sd.listShifts.map(s => (s.id, s.listNurses.groupBy(sn => sn.role).map(t => (t._1, t._2.size)))).toMap
//////val spMap = sp.map(sp => (sp.period, sp.requirements.map(req => (req.role, req.numberOfNurses)).toMap)).toMap
//////
//////val areEqual = sdMap.equals(spMap)
//////
//////
////////
//////////
//////////def duplicateN2[A](n: Int, l: List[A]):List[A] = {
//////////  l flatMap { List. List.fill(n)(_) }
//////////}
////////
//////////val nurses = genNurses.sample.get
//////////val constraint = genConstraint.sample.get
//////////
//////////val tuples = genSchedulingPeriodsV2(nurses, constraint).sample.get
////////
////////val nurse =
////////  for
////////    name <- NonEmptyStringST.from("n00001")
////////    seniority <- Seniority.from(1)
////////    role <- NurseRole.from("roleA")
////////  yield Nurse(name, seniority, Seq[NurseRole](role))
////////
////////val nurses = List[Nurse](nurse.getOrElse(null))
////////val constraintEither =
////////  for
////////    n1 <- Number.from(3)
////////    n2 <- Number.from(1)
////////  yield Constraint(n1, n2)
////////
////////val constraint = constraintEither.getOrElse(null)
////////val sp = genSchedulingPeriods(nurses, constraint).sample.get
////////
////////
////////val periodsGen = genPeriodsList.sample.get
////////val availableNurses = replicateList(min(periodsGen.size, constraint.maxShiftsPerDay.to), nurses)
////////val index = List.range(0, periodsGen.size)
////////val anSize = availableNurses.size
////////val pgSize = periodsGen.size
////////val isPGGreater = pgSize > anSize
////////val increment = max(availableNurses.size, periodsGen.size) / min(availableNurses.size, periodsGen.size)
////////val listOfNursesForAllPeriods = index.map(i => availableNurses.slice(i * increment, (i + 1) * increment))
//////////ARG_0: ScheduleInfo(List(Nurse(n00001,1,List(roleA))),Constraint(3,1),Lis
//////////  t(PeriodPreference(Nurse(n00001,1,List(roleA)),2,afternoon), PeriodPrefer
//////////  ence(Nurse(n00001,1,List(roleA)),1,morning)),List(DayPreference(Nurse(n00
//////////001,1,List(roleA)),-1,7)),List(SchedulingPeriod(night,00:00,08:00,List())
//////////, SchedulingPeriod(morning,08:00,16:00,List()), SchedulingPeriod(afternoo
//////////  n,16:00,00:00,List())))
////////
//////////val periodsGen = genPeriodsList.sample.get
//////////val availableNurses = replicateList(min(periodsGen.size, constraint.maxShiftsPerDay.to), nurses)
//////////val index = List.range(0, periodsGen.size)
//////////val increment = availableNurses.size / periodsGen.size
////////////val test = index.map(i => (i * increment, (i + 1) * increment))
////////////val test2 = test.map( (start, end) => availableNurses.slice(start, end))
//////////val nursesForPeriods = index.map(i => availableNurses.slice(i * increment, (i + 1) * increment))
////////
//////////duplicateN2(2, nurses)
//////////replicateList(2,nurses)
//////////
//////////val nurseRoles = nurses.map(nurse => nurse.roles).flatten
////////////genDayOfWeekList.sample.get
////////////genDayOfWeekMap.sample.get
//////////
//////////genDayPrefsPerDay(WeekDay.monday.value, nurses).sample.get
//////////genDayPreferences(nurses).sample.get
//////////
//////////genSchedulingPeriods(nurseRoles, 5).sample.get
//////////
//////////genScheduleInfo.sample.get
//////////
////////
////////
//////////
////////////genConstraint.sample.get
////////////(1 to 10).toList.map(_ => genConstraint.sample.get)
//////////
//////////val nurses = genNurses.sample.get
////////////(1 to 10).toList.map(i => genNurse(i).sample.get)
////////////val randomNumber = Gen.chooseNum( MIN_NURSES, MAX_NURSES).sample.get
////////////val idList = List.range(0 , randomNumber)
////////////
////////////
////////////val numberOfRoles = Gen.chooseNum(MIN_NURSE_ROLES, MAX_NURSE_ROLES).sample.get
////////////val alphabet = List.range(MIN_ASCII_LETTER, MAX_ASCII_LETTER).map(ascii => ascii.toChar)
////////////val sampleAlphabet = alphabet.take(numberOfRoles * 2)
////////////val roleLettersList = Gen.pick(numberOfRoles, sampleAlphabet).sample.get.sorted //alphabet.take(numberOfRoles)
////////////
////////////WeekDay.values.toList.map(dayOfWeek => genDayPreferencesPerDay(dayOfWeek.value, nurses).sample.get)
////////////
////////////val numberOfPreferences = Gen.chooseNum(MIN_DAY_PREFERENCES, MAX_DAY_PREFERENCES).sample.get
////////////val nursesList = Gen.pick(numberOfPreferences, nurses).sample.get
////////////val genDayPreferences = nursesList.map(nurse => genDayPreferencePerDay(DayOfWeek.fromOne, nurse).sample.get)
////////////val genDayPreferencesV2 = nursesList.map(nurse => genDayPreferencePerDay(DayOfWeek.fromOne, nurse))
////////////
////////////val dayPreferences = Gen.sequence[List[DayPreference], DayPreference](genDayPreferencesV2).sample.toList
//////////
////////////val periods = List[NonEmptyStringST](NonEmptyStringST.from("morning").fold(_ => "Error", p => p), NonEmptyStringST.from("afternoon").fold(_ => "Error", p => p), NonEmptyStringST.from("night").fold(_ => "Error", p => p))
//////////
//////////generators.DayPreferencesGenerator.genPreferences(nurses).sample.get
////////////generators.PeriodPreferencesGenerator.genPreferences(nurses, periods).sample.get