package generators

import domain.SimpleTypes.{NonEmptyStringST, PeriodTime}
import org.scalacheck.Gen

object PeriodGenerator:
  val MIN_PERIODS = 1
  val MAX_PERIODS = 10

  def createPeriodTime(index: Int, hour: Int, minutes: Int, seconds: Int) : PeriodTime =
    val baseTime = PeriodTime.fromZero
    val startHour = hour * index
    val startMinutes = minutes * index
    val startSeconds = seconds * index
    baseTime.add(startHour, startMinutes, startSeconds)

  def genPeriods: Gen[List[(NonEmptyStringST, PeriodTime, PeriodTime)]] =
    for
      numberOfPeriods <- Gen.chooseNum(MIN_PERIODS, MAX_PERIODS)
      idList = List.range(0, numberOfPeriods)
      periodPercentage = 24 / (numberOfPeriods * 1.0)
      hoursInPeriod = math.floor(periodPercentage).toInt
      minutesInPeriod = math.round((periodPercentage - hoursInPeriod) * 60.0).toInt
      secondsInPeriod = 0
      genPeriodList = idList.map(index => genSinglePeriod(index, hoursInPeriod, minutesInPeriod, secondsInPeriod))
      nursesList <- Gen.sequence[List[(NonEmptyStringST, PeriodTime, PeriodTime)], (NonEmptyStringST, PeriodTime, PeriodTime)](genPeriodList)
    yield nursesList

  def genSinglePeriod(index: Int, hour: Int, minutes: Int, seconds: Int): Gen[(NonEmptyStringST, PeriodTime, PeriodTime)] =
    for
      name <- NonEmptyStringST.from("SP" + "%02d".format(index + 1)).fold(_ => Gen.fail, Gen.const)
      startTime = createPeriodTime(index, hour, minutes, seconds)
      endTime = createPeriodTime(index + 1, hour, minutes, seconds)
    yield (name, startTime, endTime)