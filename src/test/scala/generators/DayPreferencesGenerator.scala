package generators

import domain.SimpleTypes.{DayOfWeek, Value, WeekDay}
import domain.entities.{DayPreference, Nurse}
import org.scalacheck.Gen
import scala.math.{max, min}

object DayPreferencesGenerator:
  val MIN_VALUES = -2
  val MAX_VALUES = 2
  val MIN_PREFERENCES = 1
  val MAX_PREFERENCES = 7

  def genDayOfWeekList: Gen[List[DayOfWeek]] =
    for
      num <- Gen.chooseNum(0, WeekDay.values.size);
      genDayOfWeek <- Gen.pick(num, WeekDay.values)
    yield genDayOfWeek.toList.map(weekDay => weekDay.value).sortWith{ case (d1, d2) => d1 < d2 }

  def genValue: Gen[Value] =
    for
      number <- Gen.chooseNum(MIN_VALUES, MAX_VALUES)
      value <- Value.from(number).fold(_ => Gen.fail, Gen.const)
    yield value

  def genDayPreferencePerDay(dayOfWeek: DayOfWeek, nurse: Nurse): Gen[DayPreference] =
    for
      value <- genValue
    yield DayPreference(nurse, value, dayOfWeek)

  def genDayPrefsPerDay(dayOfWeek: DayOfWeek, nurses: List[Nurse]): Gen[List[DayPreference]] =
    for
      number <- Gen.chooseNum(MIN_PREFERENCES, MAX_PREFERENCES)
      pickedNurses <- Gen.pick(min(number, nurses.size), nurses)
      dayPreferencesGen = pickedNurses.map(nurse => genDayPreferencePerDay(dayOfWeek, nurse))
      dayPreferences <- Gen.sequence[List[DayPreference], DayPreference](dayPreferencesGen)
    yield dayPreferences

  def genDayPreferences(nurses: List[Nurse]): Gen[List[DayPreference]] =
    for
      dayOfWeekGen <- genDayOfWeekList
      dayPreferencesGen = dayOfWeekGen.map(dayOfWeek => genDayPrefsPerDay(dayOfWeek, nurses))
      dayPreferences <- Gen.sequence[List[List[DayPreference]], List[DayPreference]](dayPreferencesGen)
    yield dayPreferences.flatten