package generators

import org.scalacheck.Gen
import domain.entities.*
import domain.SimpleTypes.*
import generators.ConstraintGenerator.*
import generators.DayPreferencesGenerator.*
import generators.ExtraNursesGenerator.*
import generators.NurseGenerator.*
import generators.PeriodGenerator.*
import generators.PeriodPreferencesGenerator.*
import generators.SchedulingPeriodGenerator.*

import scala.math.{max, min}

object ScheduleInfoGenerator:

  def genScheduleInfo: Gen[ScheduleInfo] =
    for
      periods <- genPeriods
      constraint <- genConstraint
      nurses <- genNurses(periods.size)
      schedulingPeriods <- genSchedulingPeriods(nurses, constraint, periods)
      periodsNames = schedulingPeriods.map(sp => sp.period).distinct
      dayPreferences <- genDayPreferences(nurses)
      periodPreferences <- genPeriodPreferences(nurses, periodsNames)
      extraNurses <- genExtraNurses(schedulingPeriods, constraint, nurses)
    yield ScheduleInfo(nurses ++ extraNurses, constraint, periodPreferences, dayPreferences, schedulingPeriods)