package generators

import domain.SimpleTypes.{NonEmptyStringST, Value}
import domain.entities.{PeriodPreference, Nurse}
import org.scalacheck.Gen
import scala.math.{max, min}

object PeriodPreferencesGenerator:
  val MIN_VALUES = -2
  val MAX_VALUES = 2
  val MIN_PREFERENCES = 1
  val MAX_PREFERENCES = 7

  def genPreferencePeriodsList(periods: List[NonEmptyStringST]): Gen[List[NonEmptyStringST]] =
    for
      num <- Gen.chooseNum(0, periods.size);
      genPeriod <- Gen.pick(num, periods)
    yield genPeriod.toList

  def genValue: Gen[Value] =
    for
      number <- Gen.chooseNum(MIN_VALUES, MAX_VALUES)
      value <- Value.from(number).fold(_ => Gen.fail, Gen.const)
    yield value

  def genPeriodPreferencePerDay(period: NonEmptyStringST, nurse: Nurse): Gen[PeriodPreference] =
    for
      value <- genValue
    yield PeriodPreference(nurse, value, period)

  def genPeriodPrefsPerDay(period: NonEmptyStringST, nurses: List[Nurse]): Gen[List[PeriodPreference]] =
    for
      number <- Gen.chooseNum(MIN_PREFERENCES, MAX_PREFERENCES)
      pickedNurses <- Gen.pick(min(number, nurses.size), nurses)
      periodPreferencesGen = pickedNurses.map(nurse => genPeriodPreferencePerDay(period, nurse))
      periodPreferences <- Gen.sequence[List[PeriodPreference], PeriodPreference](periodPreferencesGen)
    yield periodPreferences

  def genPeriodPreferences(nurses: List[Nurse], periods: List[NonEmptyStringST]): Gen[List[PeriodPreference]] =
    for
      periodGen <- genPreferencePeriodsList(periods)
      periodPreferencesGen = periodGen.map(period => genPeriodPrefsPerDay(period, nurses))
      periodPreferences <- Gen.sequence[List[List[PeriodPreference]], List[PeriodPreference]](periodPreferencesGen)
    yield periodPreferences.flatten