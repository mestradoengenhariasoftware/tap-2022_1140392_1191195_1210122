package generators

import domain.entities.{Constraint, Nurse, SchedulingPeriod}
import domain.SimpleTypes.{NonEmptyStringST, NurseRole}
import org.scalacheck.Gen
import generators.NurseGenerator.*

object ExtraNursesGenerator:
  val EXTRA_NURSE_NAME_PREFIX = "ne"

  def genExtraNurses(schedulingPeriods: List[SchedulingPeriod], constraint: Constraint, nurses: List[Nurse]) : Gen[List[Nurse]] =
    val rolesMap = schedulingPeriods.map(sp => sp.requirements.map(req => (req.role, req.numberOfNurses))).flatten.groupBy(tuple => tuple._1)
    val rolesQtdMap = rolesMap.map(t => (t._1, t._2.foldLeft[(Int, Int)]((0, 0))((accum, tuple) => {
      (accum._1 + tuple._2.to, nurses.count(nurse => nurse.roles.contains(t._1)))
    })))
    val extraNursesGen = rolesQtdMap.map {case (role, qtd) => genExtraNursesList(role, (constraint.minRestDaysWeek.to * qtd._1) - (qtd._2 - qtd._1))}.toList

    for
      extraNurses <- Gen.sequence[List[List[Nurse]], List[Nurse]](extraNursesGen)
    yield extraNurses.flatten

  def genExtraNursesList(role: NurseRole, qtdNursesToGenerate: Int) : Gen[List[Nurse]] =
    val range = List.range(0, qtdNursesToGenerate)
    val nursesGen = range.map(index => genExtraNurse(role, index))
    Gen.sequence[List[Nurse], Nurse](nursesGen)

  def genExtraNurse(role: NurseRole, index: Int) : Gen[Nurse] =
    for
      name <- genNurseName(EXTRA_NURSE_NAME_PREFIX + role.to.takeRight(1), index + 1)
      seniority <- genSeniority
      roles = Seq[NurseRole](role)
    yield Nurse(name, seniority, roles)