package generators

import domain.SimpleTypes.{NonEmptyStringST, Number, NurseRole, PeriodTime}
import domain.entities.{Constraint, Nurse, NurseRequirement, SchedulingPeriod}
import org.scalacheck.Gen

import scala.math.{max, min}

object SchedulingPeriodGenerator:
  val MIN_NURSE_REQUIREMENT_NUMBER = 1

  def genSchedulingPeriods(nurses: List[Nurse], constraint: Constraint, periods: List[(NonEmptyStringST, PeriodTime, PeriodTime)]): Gen[List[SchedulingPeriod]] =
    val availableNurses = replicateList(min(periods.size, constraint.maxShiftsPerDay.to), nurses)
    val index = List.range(0, periods.size)
    val increment = availableNurses.size / periods.size
    val listOfNursesForAllPeriods = index.map(i => availableNurses.slice(i * increment, (i + 1) * increment))

    for
      schedulingPeriod <- genSchedulingPeriodsList(periods, listOfNursesForAllPeriods)
    yield schedulingPeriod

  def replicateList[A](numberOfTimes: Int, list: List[A]) : List[A] =
    numberOfTimes match
      case 0 => list
      case 1 => list
      case _ => list ++ replicateList(numberOfTimes - 1, list)

  def genSchedulingPeriodsList(periods: List[(NonEmptyStringST, PeriodTime, PeriodTime)], nursesPerPeriod: List[List[Nurse]]) :  Gen[List[SchedulingPeriod]] =
    val periodsNurses = periods.zip(nursesPerPeriod)
    val schedulingPeriodGen =
      for
        ((period, startTime, endTime), nursesForPeriods) <- periodsNurses
        schedulingPeriod = genSingleSchedulingPeriod(period, startTime, endTime, nursesForPeriods)
      yield schedulingPeriod
    Gen.sequence[List[SchedulingPeriod], SchedulingPeriod](schedulingPeriodGen)

  def genSingleSchedulingPeriod(period: NonEmptyStringST, startTime: PeriodTime, endTime: PeriodTime, nurses: List[Nurse]): Gen[SchedulingPeriod] =
    for
      allPossibleNurseRoles <- genAllPossibleNurseRoles(nurses)
      nurseRequirementsGen = allPossibleNurseRoles.map( (nurseRole, quantity) => genNurseRequirement(nurseRole, quantity))
      nurseRequirements <- Gen.sequence[Seq[NurseRequirement], NurseRequirement](nurseRequirementsGen)
    yield SchedulingPeriod(period, startTime, endTime, nurseRequirements)

  def genAllPossibleNurseRoles(nurses: List[Nurse]): Gen[Seq[(NurseRole, Int)]] =
    val pickedRolesGen = nurses.map(nurse => Gen.oneOf(nurse.roles))
    for
      pickedRoles <- Gen.sequence[List[NurseRole], NurseRole](pickedRolesGen)
      pickedRolesMap = pickedRoles.groupBy(x=>x).map(t => (t._1, t._2.size))
    yield pickedRolesMap.toSeq

  def genNurseRequirement(nurseRole: NurseRole, numberOfNurses: Int): Gen[NurseRequirement] =
    for
      value <- Gen.chooseNum( MIN_NURSE_REQUIREMENT_NUMBER, numberOfNurses)
      nurseRequirementNumber <- Number.from(value).fold(_ => Gen.fail, Gen.const)
    yield NurseRequirement(nurseRole, nurseRequirementNumber)