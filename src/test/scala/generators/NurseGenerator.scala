package generators

import domain.SimpleTypes.{NonEmptyStringST, NurseRole, Seniority}
import domain.entities.{Nurse, SchedulingPeriod}
import org.scalacheck.Gen

object NurseGenerator:
//  val MIN_NURSES = 3
  val MAX_NURSES = 10
  val MIN_SENIORITY = 1
  val MAX_SENIORITY = 5
  val MIN_ASCII_LETTER = 65
  val MAX_ASCII_LETTER = 91
  val MIN_NURSE_ROLES = 1
  val MAX_NURSE_ROLES = 5
  val NURSE_NAME_PREFIX = "n"

  def genNurseName(nursePrefix: String, nurseNumber: Int): Gen[NonEmptyStringST] =
    for
      name <- NonEmptyStringST.from(nursePrefix + "%05d".format(nurseNumber)).fold(_ => Gen.fail, Gen.const)
    yield name

  def genSeniority: Gen[Seniority] =
    for
      value <- Gen.chooseNum(MIN_SENIORITY, MAX_SENIORITY)
      seniority <- Seniority.from(value).fold(_ => Gen.fail, Gen.const)
    yield seniority

  def genRole(letter: Char): Gen[NurseRole] =
    for
      role <- NurseRole.from("role" + letter).fold(_ => Gen.fail, Gen.const)
    yield role

  // 65 - A // 90 - Z
  def genRoles: Gen[Seq[NurseRole]] =
    for
      numberOfRoles <- Gen.chooseNum(MIN_NURSE_ROLES, MAX_NURSE_ROLES)
      alphabet = List.range(MIN_ASCII_LETTER, MAX_ASCII_LETTER).map(ascii => ascii.toChar)
      sampleAlphabet = alphabet.take(numberOfRoles * 2)
      roleLettersList <- Gen.pick(numberOfRoles, sampleAlphabet)
      genRolesList = roleLettersList.sorted.map(genRole)
      rolesList <- Gen.sequence[Seq[NurseRole], NurseRole](genRolesList)
    yield rolesList

  def genNurse(nurseNumber: Int): Gen[Nurse] =
    for
      name <- genNurseName(NURSE_NAME_PREFIX, nurseNumber)
      seniority <- genSeniority
      roles <- genRoles
    yield Nurse(name, seniority, roles)

  def genNurses(minNumberOfNurses: Int): Gen[List[Nurse]] =
    for
      randomNumber <- Gen.chooseNum(minNumberOfNurses, MAX_NURSES)
      idList = List.range(0, randomNumber)
      genNursesList = idList.map(number => genNurse(number + 1))
      nursesList <- Gen.sequence[List[Nurse], Nurse](genNursesList)
    yield nursesList