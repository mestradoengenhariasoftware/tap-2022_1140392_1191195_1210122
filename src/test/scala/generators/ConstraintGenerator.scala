package generators

import domain.SimpleTypes.Number
import domain.entities.Constraint
import org.scalacheck.Gen

object ConstraintGenerator :

  val MIN_REST_DAYS = 1
  val MAX_REST_DAYS = 5
  val MIN_SHIFTS_PER_DAYS = 1
  val MAX_SHIFTS_PER_DAYS = 3

  def genNumber(min: Int, max: Int): Gen[Number] =
    for
      value <- Gen.chooseNum(min, max)
      number <- Number.from(value).fold(_ => Gen.fail, Gen.const)
    yield number

  def genConstraint: Gen[Constraint] =
    for
      minRestDaysWeek <- genNumber(MIN_REST_DAYS, MAX_REST_DAYS)
      maxShiftsPerDay <- genNumber(MIN_SHIFTS_PER_DAYS, MAX_SHIFTS_PER_DAYS)
    yield Constraint(minRestDaysWeek, maxShiftsPerDay)