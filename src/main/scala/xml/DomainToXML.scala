package xml

import domain.entities.{ShiftNurse, ScheduleDay, Shift, ShiftSchedule}
import scala.xml.*

object DomainToXML:
  def toResult(res: ShiftSchedule) : Elem =
    shiftSchedule(res)

  private def shiftSchedule(ss: ShiftSchedule) : Elem =
    <shiftSchedule xmlns="http://www.dei.isep.ipp.pt/tap-2022" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2022 ../../schedule.xsd ">
      { ss.days.map(sd => scheduleDay(sd))}
    </shiftSchedule>

  private def scheduleDay(sd: ScheduleDay) : Elem =
    <day id={ sd.id.to.toString }>
      { sd.listShifts.map(s => shift(s))}
    </day>

  private def shift(s: Shift) : Elem =
    <shift id={ s.id.to.toString }>
      { s.listNurses.map(n => nurse(n))}
    </shift>

  private def nurse(sn: ShiftNurse) : Elem =
    <nurse name={ sn.nurse.name.to.toString } role={ sn.role.toString }/>