package xml

import domain.*
import domain.DomainError.*
import domain.entities.*
import domain.SimpleTypes.*

import scala.xml.*
import xml.XML.*

import scala.util

object XMLtoDomain:
  def scheduleInfo(xml: Elem) : Result[ScheduleInfo] =
    for
      nurses <- traverse ( (xml \\ "nurse"), nurse )
      eConstraint <- fromNode(xml, "constraints")
      constraint <- constraints(eConstraint)
      periodPreferences <- traverse ( (xml \\ "periodPreference"), periodPreference(nurses) )
      dayPreferences <- traverse ( (xml \\ "dayPreference"), dayPreference(nurses) )
      schedulingPeriods <- traverse ( (xml \\ "schedulingPeriod"), schedulingPeriod(nurses) )
    yield ScheduleInfo(nurses, constraint, periodPreferences, dayPreferences, schedulingPeriods)

  private def nurse(xml: Node): Result[Nurse] =
    for
      strNurseId <- fromAttribute(xml, "name")
      nurseId <- NonEmptyStringST.from(strNurseId)
      strSeniority <- fromAttribute(xml, "seniority")
      seniority <- Seniority.from(strSeniority)
      roles <- traverse ( (xml \\ "nurseRole"), nurseRole )
    yield Nurse(nurseId, seniority, roles)

  private def nurseRole(xml: Node) : Result[NurseRole] =
    for
      strNurseRole <- fromAttribute(xml, "role")
      nurseRole <- NurseRole.from(strNurseRole)
    yield nurseRole

  private def constraints(xml: Node) : Result[Constraint] =
    for
      strMinRestDaysWeek <- fromAttribute(xml, "minRestDaysPerWeek")
      minRestDaysWeek <- Number.from(strMinRestDaysWeek)
      strMaxShiftsPerDay <- fromAttribute(xml, "maxShiftsPerDay")
      maxShiftsPerDay <- Number.from(strMaxShiftsPerDay)
    yield Constraint(minRestDaysWeek, maxShiftsPerDay)

  private def periodPreference(nurses: List[Nurse])(xml: Node) : Result[PeriodPreference] =
    for
      strNurseId <- fromAttribute(xml, "nurse")
      nurseId <- NonEmptyStringST.from(strNurseId)
      nurse <- FindNurseById(nurses, nurseId)
      strValue <- fromAttribute(xml, "value")
      value <- Value.from(strValue)
      strPeriod <- fromAttribute(xml, "period")
      period <- NonEmptyStringST.from(strPeriod)
    yield PeriodPreference(nurse, value, period)

  private def dayPreference(nurses: List[Nurse])(xml: Node) : Result[DayPreference] =
    for
      strNurseId <- fromAttribute(xml, "nurse")
      nurseId <- NonEmptyStringST.from(strNurseId)
      nurse <- FindNurseById(nurses, nurseId)
      strValue <- fromAttribute(xml, "value")
      value <- Value.from(strValue)
      strDay <- fromAttribute(xml, "day")
      day <- DayOfWeek.from(strDay)
    yield DayPreference(nurse, value, day)

  private def schedulingPeriod(nurses: List[Nurse]) (xml: Node) : Result[SchedulingPeriod] =
    val result = for
      strPeriod <- fromAttribute(xml, "id")
      period <- NonEmptyStringST.from(strPeriod)
      strStartTime <- fromAttribute(xml, "start")
      startTime <- PeriodTime.from(strStartTime)
      strEndTime <- fromAttribute(xml, "end")
      endTime <- PeriodTime.from(strEndTime)
      requirements <- traverse ( (xml \\ "nurseRequirement"), nurseRequirement(nurses) )
    yield SchedulingPeriod(period, startTime, endTime, requirements)
      result

  private def nurseRequirement(nurses: List[Nurse]) (xml: Node) : Result[NurseRequirement] =
    val allRoles = nurses.flatten(nurse=> nurse.roles).distinct
    for
      strNurseRoleId <- fromAttribute(xml, "role")
      nurseRoleId <- NurseRole.from(strNurseRoleId)
      nurseRole <- FindNurseRoleById(allRoles, nurseRoleId)
      strNumber <- fromAttribute(xml, "number")
      number <- Number.fromStrict(strNumber)
    yield NurseRequirement(nurseRole, number)

  private def FindNurseById(nurse: List[Nurse], nurseId: NonEmptyStringST) : Result[Nurse] =
    nurse.find(n => n.name == nurseId).fold(Left(InvalidNurseId(nurseId.to)))(n => Right(n))

  private def FindNurseRoleById(nurseRoles: List[NurseRole], nurseRoleId: NurseRole) : Result[NurseRole] =
    nurseRoles.find(nr => nr == nurseRoleId).fold(Left(UnknownRequirementRole(nurseRoleId.to)))(n => Right(n))
