import domain.SimpleTypes.{NonEmptyStringST, PeriodTime}

import java.time.*

final case class NurseRoleTest (role: String)
final case class NurseTest (name: String, seniority: Int, roles: Seq[NurseRoleTest])
val nurse1 = NurseTest ("n1", 5,  Seq(NurseRoleTest("A"), NurseRoleTest("B")))
val nurse2 = NurseTest ("n2", 4,  Seq(NurseRoleTest("B")))
val nurse3 = NurseTest ("n3", 2,  Seq(NurseRoleTest("A"), NurseRoleTest("B"), NurseRoleTest("C")))

val allNursesTest = List(nurse1, nurse2, nurse3)

val allRoles = allNursesTest.flatten(nurse=> nurse.roles).distinct

val t1 = NonEmptyStringST.from("assa")
val t2 = NonEmptyStringST.from(" assa ")
val t3 = NonEmptyStringST.from("       ")
val t4 = NonEmptyStringST.from("")


val n1 = NurseTest ("n1", 5,  Seq(NurseRoleTest("A")))
val n2 = NurseTest ("n2", 5,  Seq(NurseRoleTest("B")))
val n3 = NurseTest ("n3", 5,  Seq(NurseRoleTest("A"), NurseRoleTest("B")))
val n4 = NurseTest ("n4", 5,  Seq(NurseRoleTest("A"), NurseRoleTest("B")))

val allNTest = List(n1, n2, n3, n4)
val nurseFirstRole = allNTest.map(nurse=> (nurse.name, nurse.roles.head.role)).distinct



final case class ShiftTest(id: String, listNurses: Seq[NurseTest])
val shift1 = ShiftTest("morning", Seq(NurseTest ("n1", 1,  Seq(NurseRoleTest("A"))), NurseTest ("n2", 1,  Seq(NurseRoleTest("B"))), NurseTest ("n3", 1,  Seq(NurseRoleTest("A"))), NurseTest ("n4", 1,  Seq(NurseRoleTest("B")))))
val shift2 = ShiftTest("afternoon", Seq(NurseTest ("n1", 1,  Seq(NurseRoleTest("A"))), NurseTest ("n2", 1,  Seq(NurseRoleTest("B")))))

val shifts = Seq(shift1, shift2)

val test = shifts.map(shift => (1, shift.id, shift.listNurses))

val test1 = shifts.map(shift => shift.listNurses.map(nurse => (1, shift.id, nurse))).flatten

val test2 = shift1.listNurses.map(nurse => (1, shift1.id, nurse))


import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

val time1 = "08:45:22"
val time2 = "16:00:00"
val time3 = "00:00:00"
val test = LocalTime.MIN
val formatter = DateTimeFormatter.ofPattern("HH:mm:ss")
val dateTime1 = LocalTime.parse(time1, formatter)
val dateTime2 = LocalTime.parse(time2, formatter)
val dateTime3 = LocalTime.parse(time3, formatter)


val ValidTime = "14:00:00"
PeriodTime.from(ValidTime)

val ValidTime1 = "14:00:10"
PeriodTime.from(ValidTime1)
