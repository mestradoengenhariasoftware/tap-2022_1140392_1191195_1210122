package domain

import domain.DomainError.*
import domain.entities.{Constraint, DayPreference, Nurse, NurseRequirement, PeriodPreference, ScheduleDay, ScheduleInfo, SchedulingPeriod, Shift, ShiftNurse, ShiftSchedule}
import domain.Result
import domain.SimpleTypes.{DayOfWeek, NonEmptyStringST, Number, NurseRole, Value}

object ScheduleService extends ScheduleOps:
  def createNurseSchedule(s: ScheduleInfo): Result[ShiftSchedule] =
    val availableNurses = s.nurses.map(n => (n, s.constraint.maxShiftsPerDay))
    val shiftsResult = s.schedulingPeriods.foldLeft[Result[(List[(Nurse, Number)], Seq[Shift])]](Right(availableNurses, Seq())) ((accum, sp) => {
        for
          (availableNurses, shifts) : (List[(Nurse, Number)], Seq[Shift]) <- accum
          (currentAvailableNurses, currentShift) <- calculateSchedulePeriod(sp, availableNurses)
        yield (currentAvailableNurses, shifts ++ Seq(currentShift))
      })
      shiftsResult.fold(error => Left(error), (an, shifts) => Right(ShiftSchedule(DayOfWeek.weekDays.map(day => ScheduleDay(day, shifts)))))

  private def calculateSchedulePeriod(sp: SchedulingPeriod, availableNurses: List[(Nurse, Number)]) : Result[(List[(Nurse, Number)], Shift)] =
    val result = sp.requirements.foldLeft[Result[(List[(Nurse, Number)], List[Nurse], Seq[ShiftNurse])]](Right(availableNurses, List(), Seq())) ((accum, req) => {
      for
        (availableNurses, usedNurses, shiftNurses):(List[(Nurse, Number)], List[Nurse], Seq[ShiftNurse]) <- accum
        currentShiftNurses <- calculateNurseRequirement(req, availableNurses, usedNurses)
        currentUsedNurses = currentShiftNurses.map(sn => sn.nurse)
        currentAvailableNurses = updateAvailableNurses(availableNurses, currentUsedNurses)
      yield (currentAvailableNurses, usedNurses ++ currentUsedNurses, shiftNurses ++ currentShiftNurses)
    })
    result.fold(error => Left(error), (an, us, shiftNurses) => Right(an, Shift(sp.period, shiftNurses.sortBy(nurse=> nurse.nurse.name.to))))

  private def calculateNurseRequirement(req: NurseRequirement, availableNurses: List[(Nurse, Number)], usedNurses: List[Nurse]) : Result[Seq[ShiftNurse]] =
    val nrReq = req.numberOfNurses.to
    val currentUsedNurses = availableNurses.filter{case (nurse, _) => nurse.roles.contains(req.role) && !usedNurses.contains(nurse)}.take(nrReq)

    if(currentUsedNurses.size != nrReq)
      Left(NotEnoughNurses)
    else
      Right(currentUsedNurses.map{case (nurse, _) => ShiftNurse(nurse, req.role)})

  private def updateAvailableNurses(availableNurses: List[(Nurse, Number)], currentUsed: Seq[Nurse]) : List[(Nurse, Number)] =
    val numberOne = Number.fromOne
    val numberZero = Number.fromZero
    availableNurses.map{case (nurse, nr) => {
      if(currentUsed.contains(nurse))
        (nurse, nr -  numberOne)
      else
        (nurse, nr)
    }}.filter{case (_, nr) => nr > numberZero}

/*  private def getPreferencesValuesFromNurse(s: ScheduleInfo, nurse: Nurse, dayOfWeek: DayOfWeek, periodString: NonEmptyStringST) : List[(DayPreference, PeriodPreference)] =
    for
      nurseDayPreference <- s.dayPreference.filter(dp => (dp.nurse == nurse && dp.dayOfWeek == dayOfWeek) )
      nursePeriodPreference <- s.periodPreference.filter(pd => pd.nurse == nurse && pd.periodString == periodString)
    yield (nurseDayPreference,nursePeriodPreference)*/

