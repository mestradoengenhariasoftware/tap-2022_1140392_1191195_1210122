package domain

type Result[A] = Either[DomainError,A]

enum DomainError:
  case IOFileProblem(error: String)
  case XMLError(error: String)
  case EmptyName
  case InvalidTimeFormat
  case PeriodStartTimeCanNotBeGreaterThanPeriodEndTime
  case NotEnoughNurses
  case InvalidNumber(error: Int)
  case InvalidDayOfWeek(error: Int)
  case PreferenceValueOutOfRange(error: Int)
  case SeniorityValueOutOfRange(error: Int)
  case UnknownRequirementRole(error: String)
  case InvalidNurseId(error: String)