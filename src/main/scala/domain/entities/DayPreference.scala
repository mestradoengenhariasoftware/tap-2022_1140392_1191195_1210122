package domain.entities

import domain.entities.Nurse
import domain.SimpleTypes.{Value, DayOfWeek}

final case class DayPreference(nurse: Nurse, value: Value, dayOfWeek: DayOfWeek) extends Preference