package domain.entities

import domain.entities.Nurse
import domain.SimpleTypes.NurseRole

final case class ShiftNurse (nurse: Nurse, role: NurseRole)