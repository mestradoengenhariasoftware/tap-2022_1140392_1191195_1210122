package domain.entities

import domain.SimpleTypes.{Number, NurseRole}

final case class NurseRequirement(role: NurseRole, numberOfNurses: Number)