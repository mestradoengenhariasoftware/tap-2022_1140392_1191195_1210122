package domain.entities

import domain.entities.ScheduleDay

final case class ShiftSchedule (days: Seq[ScheduleDay])