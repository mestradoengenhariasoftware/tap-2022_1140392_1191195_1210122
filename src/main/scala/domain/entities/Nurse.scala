package domain.entities

import domain.SimpleTypes.{NonEmptyStringST, NurseRole, Seniority}

final case class Nurse (name: NonEmptyStringST, seniority: Seniority, roles: Seq[NurseRole])