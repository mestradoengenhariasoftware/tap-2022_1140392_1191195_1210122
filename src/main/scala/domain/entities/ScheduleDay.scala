package domain.entities

import domain.entities.Shift
import domain.SimpleTypes.{DayOfWeek, Number}

final case class ScheduleDay(id: DayOfWeek, listShifts: Seq[Shift])
