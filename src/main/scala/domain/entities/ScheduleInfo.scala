package domain.entities

import domain.entities.{ Nurse, DayPreference, PeriodPreference, SchedulingPeriod }

final case class ScheduleInfo (nurses: List[Nurse], constraint: Constraint,
                               periodPreference: List[PeriodPreference], dayPreference: List[DayPreference],
                               schedulingPeriods: List[SchedulingPeriod])