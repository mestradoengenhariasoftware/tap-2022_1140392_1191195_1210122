package domain.entities

import domain.SimpleTypes.Number

final case class Constraint(minRestDaysWeek: Number, maxShiftsPerDay: Number)