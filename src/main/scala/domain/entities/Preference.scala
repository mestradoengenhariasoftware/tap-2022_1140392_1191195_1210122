package domain.entities

import domain.entities.Nurse
import domain.SimpleTypes.Value

trait Preference:
  def nurse: Nurse
  def value: Value