package domain.entities

import domain.entities.NurseRequirement
import domain.SimpleTypes.{NonEmptyStringST, PeriodTime}

final case class SchedulingPeriod(period: NonEmptyStringST, startTime: PeriodTime, endTime: PeriodTime, requirements: Seq[NurseRequirement])