package domain.entities

import domain.entities.ShiftNurse
import domain.SimpleTypes.NonEmptyStringST

final case class Shift(id: NonEmptyStringST, listNurses: Seq[ShiftNurse])
