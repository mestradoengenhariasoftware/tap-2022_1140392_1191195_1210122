package domain.entities

import domain.entities.{Nurse, Preference}
import domain.SimpleTypes.{Value, NonEmptyStringST}

final case class PeriodPreference(nurse: Nurse, value: Value, periodString: NonEmptyStringST) extends Preference