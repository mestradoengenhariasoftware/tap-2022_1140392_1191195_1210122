package domain

import domain.DomainError.NotEnoughNurses
import domain.SimpleTypes.{DayOfWeek, NonEmptyStringST, NurseRole}
import domain.entities.{Nurse, ScheduleDay, Shift, ShiftNurse, ShiftSchedule}

import java.awt.PageAttributes.OriginType

final case class PeriodRequirementInfo (period : NonEmptyStringST, nurse: Nurse, nurseRole: NurseRole)

final case class RequirementInfo (day : DayOfWeek, period : NonEmptyStringST, nurse: Nurse, nurseRole: NurseRole, weight : Int)

object RequirementInfo :
  // ShiftSchedule (days: Seq[ScheduleDay])
  def convertToShiftSchedule(weekRequirements : List[RequirementInfo]) : Result[ShiftSchedule] =
    val allDays = weekRequirements.map(_.day).distinct
    Right(ShiftSchedule(weekRequirements.groupBy(_.day).map(convertToScheduleDay).toSeq.sortBy(sd => allDays.indexOf(sd.id))))

  // ScheduleDay(id: DayOfWeek, listShifts: Seq[Shift])
  def convertToScheduleDay(day : DayOfWeek, dayRequirements : List[RequirementInfo]) : ScheduleDay =
    val allPeriods = dayRequirements.map(_.period).distinct
    ScheduleDay(day, dayRequirements.groupBy(_.period).map(convertToShift).toSeq.sortBy(shift => allPeriods.indexOf(shift.id)))

  // Shift(id: NonEmptyStringST, listNurses: Seq[ShiftNurse])
  def convertToShift(period : NonEmptyStringST, periodRequirements : List[RequirementInfo]) : Shift =
    Shift(period, periodRequirements.map(convertToShiftNurse).sortBy(_.nurse.name.to))

  // ShiftNurse (nurse: Nurse, role: NurseRole)
  def convertToShiftNurse(nurseRequirement : RequirementInfo) : ShiftNurse =
    ShiftNurse(nurseRequirement.nurse, nurseRequirement.nurseRole)
