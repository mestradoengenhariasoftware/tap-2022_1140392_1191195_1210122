package domain

import scala.annotation.targetName
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import domain.DomainError.*

import scala.util.Try
import scala.language.postfixOps

object SimpleTypes:

  private def toOptInt(str: String) = Try(str.toInt).toOption

  // -----------------
  //Nurse name and period. They are all strings with just the condition to not be empty.
  // -----------------
  opaque type NonEmptyStringST = String

  object NonEmptyStringST:
    def from(s: String): Result[NonEmptyStringST] =
      if(!s.trim.isEmpty)
        Right(s)
      else
        Left(EmptyName)

  extension(str: NonEmptyStringST)
    @targetName("NonEmptyStringSTTo")
    def to: String= str

    @targetName("NonEmptyStringSTIsEqual")
    def isEqual(other_nonEmptyStringST: NonEmptyStringST): Boolean = str.equals(other_nonEmptyStringST)

  // -----------------
  //Nurse Seniority
  // -----------------
  opaque type Seniority = Int

  object Seniority:
    def from(i: Int): Result[Seniority] =
      if (i >= 1 && i <= 5)
        Right(i)
      else
        Left(SeniorityValueOutOfRange(i))

    @targetName("seniorityFromString")
    def from(str: String) : Result[Seniority] =
      toOptInt(str).fold(Left(XMLError("Invalid Seniority format")))(from)

    def fromOne: Seniority = 1

  extension (int: Seniority)
    @targetName("seniorityTo")
    def to: Int = int

    @targetName("seniority>")
    def >(x: Seniority): Boolean = int > x

    @targetName("seniority<")
    def <(x: Seniority): Boolean = int < x

    @targetName("seniorityIsEqual")
    def isEqual(other_seniority: Seniority): Boolean = int == other_seniority

  // -----------------
  //Nurse Role
  // -----------------
  opaque type NurseRole = String

  object NurseRole:
    def from(s: String): Result[NurseRole] =
      if(!s.trim.isEmpty)
        Right(s)
      else
        Left(EmptyName)

  extension(x: NurseRole)
    @targetName("NurseRoleTo")
    def to: String= x

    @targetName("NurseRoleIsEqual")
    def isEqual(other_nurseRole: NurseRole): Boolean = x.equals(other_nurseRole)

  // -----------------
  //Preference Value
  // -----------------
  opaque type Value = Int

  object Value:
    def from(i: Int): Result[Value] =
      if (i >= -2 && i <= 2)
        Right(i)
      else
        Left(PreferenceValueOutOfRange(i))

    @targetName("valueFromString")
    def from(str: String) : Result[Value] =
      toOptInt(str).fold(Left(XMLError("Invalid Value format")))(from)

    def fromDefault: Value = 0

  extension (int: Value)
    @targetName("valueTo")
    def to: Int = int

    @targetName("value>")
    def >(x: Value): Boolean = int > x

    @targetName("value<")
    def <(x: Value): Boolean = int < x

    @targetName("seniorityMul<")
    def *(x: Seniority): Int = int * x

    @targetName("valueIsEqual")
    def isEqual(other_value: Value): Boolean = int == other_value

  // -----------------
  //Nurses Requirement Number, minRestDaysPerWeek and max ShiftsPerDay
  // -----------------
  opaque type Number = Int

  object Number:
    def from(i: Int): Result[Number] =
      if (i >= 0)
        Right(i)
      else
        Left(InvalidNumber(i))

    def fromStrict(i: Int): Result[Number] =
      if (i > 0)
        Right(i)
      else
        Left(InvalidNumber(i))

    @targetName("numberFromString")
    def from(str: String) : Result[Number] =
      toOptInt(str).fold(Left(XMLError("Invalid Number format")))(from)

    @targetName("numberStrictFromString")
    def fromStrict(str: String) : Result[Number] =
      toOptInt(str).fold(Left(XMLError("Invalid Number format")))(from)

    def fromZero: Number = 0
    def fromOne: Number = 1

  extension (int: Number)
    @targetName("numberTo")
    def to: Int = int

    @targetName("number>")
    def >(x: Number): Boolean = int > x

    @targetName("number<")
    def <(x: Number): Boolean = int < x

    @targetName("number-")
    def -(x: Number): Number = int - x

    @targetName("number+")
    def +(x: Number): Number = int + x

    @targetName("numberIsEqual")
    def isEqual(other_number: Number): Boolean = int == other_number

  // -----------------
  //Scheduling Period Start Time and End Time
  // -----------------
  opaque type PeriodTime =  LocalTime

  object PeriodTime:
    def from(t: String): Result[PeriodTime] =
      val formatter = DateTimeFormatter.ofPattern("HH:mm:ss")
      val time = Try(LocalTime.parse(t, formatter)).toOption
      time.fold(Left(InvalidTimeFormat))(newTime => Right(newTime))

    def from(hour: Int, minutes: Int, seconds: Int): Result[PeriodTime] =
      val str = "%02d".format(hour) + ":" + "%02d".format(minutes) + ":" + "%02d".format(seconds)
      from(str)

    def fromZero: PeriodTime =
      val formatter = DateTimeFormatter.ofPattern("HH:mm:ss")
      LocalTime.parse("00:00:00", formatter)

    extension(x: PeriodTime)
      @targetName("TimeToTime")
      def to: LocalTime = x

      @targetName("TimeToString")
      def toString: String = x.toString

      @targetName("timeIsEqual")
      def isEqual(other_time: PeriodTime): Boolean = x.equals(other_time)

      @targetName("addTime")
      def add(hour: Int, minutes: Int, seconds: Int): PeriodTime = x.plusHours(hour.toLong).plusMinutes(minutes.toLong).plusSeconds(seconds.toLong)

      @targetName("timeCompareTo")
      def compareTo(other_time: PeriodTime): Int = x.compareTo(other_time)

      @targetName("isAfter")
      def isAfter(other_time: PeriodTime): Boolean = x.isAfter(other_time)

  // -----------------
  //Day of The Week
  // -----------------
  opaque type DayOfWeek = Int

  enum WeekDay(val value: DayOfWeek):
    case monday extends WeekDay(1)
    case tuesday extends WeekDay(2)
    case wednesday extends WeekDay(3)
    case thursday extends WeekDay(4)
    case friday extends WeekDay(5)
    case saturday extends WeekDay(6)
    case sunday extends WeekDay(7)

  object DayOfWeek:
    def from(i: Int): Result[DayOfWeek] =
      if (i >= 1 && i <= 7)
        Right(i)
      else
        Left(InvalidDayOfWeek(i))

    @targetName("dayOfWeekFromString")
    def from(str: String) : Result[DayOfWeek] =
      toOptInt(str).fold(Left(XMLError("Invalid Number format")))(from)

    def fromOne: DayOfWeek = 1
    def weekDays: List[DayOfWeek] = List(WeekDay.monday.value, WeekDay.tuesday.value, WeekDay.wednesday.value, WeekDay.thursday.value, WeekDay.friday.value, WeekDay.saturday.value, WeekDay.sunday.value)

  extension (int: DayOfWeek)
    @targetName("DayOfWeekTo")
    def to: Int = int

    @targetName("DayOfWeekIsEqual")
    def isEqual(other_dayOfWeek: DayOfWeek): Boolean = int == other_dayOfWeek

    @targetName("DayOfWeek>")
    def >(x: DayOfWeek): Boolean = int > x

    @targetName("DayOfWeek<")
    def <(x: DayOfWeek): Boolean = int < x