package domain

import domain.DomainError.NotEnoughNurses
import domain.SimpleTypes.{DayOfWeek, NonEmptyStringST, Number, NurseRole, WeekDay}
import domain.entities.{Constraint, DayPreference, Nurse, NurseRequirement, PeriodPreference, Preference, ScheduleDay, ScheduleInfo, SchedulingPeriod, ShiftSchedule}
import domain.{PeriodRequirementInfo, RequirementInfo, Result}

import scala.annotation.tailrec
import scala.collection.immutable.{List, Map}

object ScheduleServiceV2 extends ScheduleOps:
  val DAY_PREFERENCE_MULTIPLIER = 2
  val PERIOD_PREFERENCE_MULTIPLIER = 1

  def createNurseSchedule(s: ScheduleInfo): Result[ShiftSchedule] =
    val singleBaseDayCombination =  generateCombinationsForAllPeriods(s.nurses, s.schedulingPeriods, s.constraint)

    singleBaseDayCombination.fold(error => Left(error), baseDayCombination =>
    {
      val periodReqComb : List[List[PeriodRequirementInfo]] =
        baseDayCombination.map(combByPeriodList => {
          combByPeriodList.flatMap((period, combByRole) => {
            combByRole.map((nurse, role) => PeriodRequirementInfo(period, nurse, role))
          })
        })

      val nursesPreferenceWeight = getNursesPreferenceWeight(s)
      val weekPeriodsComb = checkDayCombinations(periodReqComb, 7 - s.constraint.minRestDaysWeek.to)
      val allDayCombinationsResult = convertToReqInfo(weekPeriodsComb, nursesPreferenceWeight)

//      val allDayCombinationsResult = generateCombinationsForAllDays(periodReqComb, nursesPreferenceWeight, s.constraint)

      allDayCombinationsResult.fold(error => Left(error), allDayCombinations => {
        val t = allDayCombinations.map(weekReqList => {
          val weekWeight = getTotalWeight(weekReqList)
          (weekReqList, weekWeight)
        }).sortBy(_._2)(Ordering[Int].reverse)
        val bestDayCombinationsWeighted = allDayCombinations.map(weekReqList => {
          val weekWeight = getTotalWeight(weekReqList)
          (weekReqList, weekWeight)
        }).sortBy(_._2)(Ordering[Int].reverse).head

        RequirementInfo.convertToShiftSchedule(bestDayCombinationsWeighted._1)
      })
    })

  def getTotalWeight(weekRequirements : List[RequirementInfo]) : Int =
    weekRequirements.map(_.weight).sum

  def generateCombinationsForAllDays(baseDayCombination : List[List[PeriodRequirementInfo]], nursesPreferenceWeight : Map[(DayOfWeek, NonEmptyStringST, Nurse), Int], constraint: Constraint) : Result[List[List[RequirementInfo]]] =
    generateCombinationsForAllDaysRecursive(WeekDay.values.map(_.value).toList, baseDayCombination, nursesPreferenceWeight, constraint, Right(List(List())))

  def generateCombinationsForAllDaysRecursive(daysOfWeek : List[DayOfWeek], baseDayCombination : List[List[PeriodRequirementInfo]], nursesPreferenceWeight : Map[(DayOfWeek, NonEmptyStringST, Nurse), Int], constraint: Constraint, accumResult : Result[List[List[RequirementInfo]]]) : Result[List[List[RequirementInfo]]] =
    if(daysOfWeek.isEmpty || accumResult.isLeft)
      accumResult
    else
      val currentAccum =
        for
          nurseCombinations <- generateCombinationsForSingleDay(daysOfWeek.head, baseDayCombination, nursesPreferenceWeight)
          accum <- accumResult
          newAccum <- combineDaySets(accum, nurseCombinations, constraint, baseDayCombination)
        yield newAccum

      generateCombinationsForAllDaysRecursive(daysOfWeek.tail, baseDayCombination, nursesPreferenceWeight, constraint, currentAccum)

  /*
    Given the list of all combinations for a single day
    tries to generate all possible combinations for it
    if it is not possible it returns the NotEnoughNurses error
  */
  def generateCombinationsForSingleDay(dayOfWeek : DayOfWeek, baseDayCombination : List[List[PeriodRequirementInfo]], nursesPreferenceWeight : Map[(DayOfWeek, NonEmptyStringST, Nurse), Int]) : Result[List[List[RequirementInfo]]] =
    val result : List[List[RequirementInfo]] =
      baseDayCombination.map(combByPeriodList => {
        combByPeriodList.map(periodReq => {
          val nurseWeight = getNursePreferenceWeight(periodReq.nurse, dayOfWeek, periodReq.period, nursesPreferenceWeight)
          RequirementInfo(dayOfWeek, periodReq.period, periodReq.nurse, periodReq.nurseRole, nurseWeight)
        })
      })
    Right(result)

  def getNursesPreferenceWeight(s: ScheduleInfo) : Map[(DayOfWeek, NonEmptyStringST, Nurse), Int] =
    val periods = s.schedulingPeriods.map(list => list.period)
    val allDaysOfWeek = WeekDay.values.map(_.value).toList
    val result : List[((DayOfWeek, NonEmptyStringST, Nurse), Int)] =
      for
        day <- allDaysOfWeek
        period <- periods
        nurse <- s.nurses
        nurseWeight = calculateNursePreferenceWeight(nurse, day, period, s)
      yield ((day, period, nurse), nurseWeight)
    result.toMap

  /*
      If both are negative / positive they are both included
      If only is negative it is only used the negative
   */
  def calculateNursePreferenceWeight(nurse : Nurse, day : DayOfWeek, period : NonEmptyStringST, s: ScheduleInfo) : Int =
    val dayPref = getPreferenceValue(nurse, s.dayPreference, (pref : DayPreference) => pref.dayOfWeek == day, DAY_PREFERENCE_MULTIPLIER)
    val periodPref = getPreferenceValue(nurse, s.periodPreference, (pref : PeriodPreference) => pref.periodString == period, PERIOD_PREFERENCE_MULTIPLIER)
    val dayPrefPositive = dayPref >= 0
    val periodPrefPositive = periodPref >= 0

    if(dayPrefPositive == periodPrefPositive)
      // Both are positive or negative
      dayPref + periodPref
    else if(!dayPrefPositive)
      // Day Preference is negative
      dayPref
    else
      // period Preference is negative
      periodPref

  def getPreferenceValue[A <: Preference](nurse : Nurse, preferences : List[A], condition : A => Boolean, multiplier : Int) : Int =
    if(!preferences.isEmpty && preferences.exists(pref => pref.nurse == nurse && condition(pref)))
      preferences.filter(pref => pref.nurse == nurse && condition(pref)).head.value * nurse.seniority * multiplier
    else
      0

  def getNursePreferenceWeight(nurse : Nurse, day : DayOfWeek, period : NonEmptyStringST, nursesPreferenceWeight : Map[(DayOfWeek, NonEmptyStringST, Nurse), Int]) : Int =
    nursesPreferenceWeight((day, period, nurse))

  /*
    Generates all possible combinations to solve all given scheduling periods
  */
  def generateCombinationsForAllPeriods(nurses : List[Nurse], schedulingPeriods : List[SchedulingPeriod], constraint: Constraint) :  Result[List[List[(NonEmptyStringST, List[(Nurse, NurseRole)])]]] =
    generateCombinationsForAllPeriodsRecursive(nurses, schedulingPeriods, constraint, Right(List(List())))

  /*
    Recursive method
    Generates all possible combinations to solve all given scheduling periods
  */
  def generateCombinationsForAllPeriodsRecursive(nurses : List[Nurse], schedulingPeriods : List[SchedulingPeriod], constraint: Constraint, accumResult : Result[List[List[(NonEmptyStringST, List[(Nurse, NurseRole)])]]]) :  Result[List[List[(NonEmptyStringST, List[(Nurse, NurseRole)])]]] =
    if(schedulingPeriods.isEmpty || accumResult.isLeft)
      accumResult
    else
      val currentAccum =
        for
          nurseCombinations <- generateCombinationsForSinglePeriod(nurses, schedulingPeriods.head)
          accum <- accumResult
          newAccum <- combinePeriodSets(accum, nurseCombinations, constraint)
        yield newAccum

      generateCombinationsForAllPeriodsRecursive(nurses, schedulingPeriods.tail, constraint, currentAccum)

  /*
    Given the list of all nurses and a single scheduling period
    tries to generate all possible combinations for it
    if it is not possible it returns the NotEnoughNurses error
  */
  def generateCombinationsForSinglePeriod(nurses : List[Nurse], schedulingPeriod: SchedulingPeriod) :  Result[List[(NonEmptyStringST, List[(Nurse, NurseRole)])]] =
    for
      reqCombinations <- generateCombinationsForAllRequirements(nurses, schedulingPeriod.requirements)
      newList = reqCombinations.map(subList => (schedulingPeriod.period, subList))
    yield newList

  /*
    Generates all possible combinations to solve all given requirements by invoking a recursive method
  */
  def generateCombinationsForAllRequirements(nurses : List[Nurse], reqs : Seq[NurseRequirement]) : Result[List[List[(Nurse, NurseRole)]]] =
    generateCombinationsForAllRequirementsRecursive(nurses, reqs, Right(List(List())))

  /*
    Recursive method
    Generates all possible combinations to solve all given requirements
  */
  def generateCombinationsForAllRequirementsRecursive(nurses : List[Nurse], reqs : Seq[NurseRequirement], accumResult : Result[List[List[(Nurse, NurseRole)]]]) : Result[List[List[(Nurse, NurseRole)]]] =
    if(reqs.isEmpty || accumResult.isLeft)
      accumResult
    else
      val currentAccum =
        for
          nurseCombinations <- generateCombinationsForSingleRequirement(nurses, reqs.head)
          accum <- accumResult
          newAccum <- combineSets(accum, nurseCombinations)
        yield newAccum

      generateCombinationsForAllRequirementsRecursive(nurses, reqs.tail, currentAccum)

  /*
    Given the list of all nurses and a single requirement
    tries to generate all possible combinations to meet the requirement
    if it is not possible it returns the NotEnoughNurses error
  */
  def generateCombinationsForSingleRequirement(nurses : List[Nurse], req : NurseRequirement) : Result[List[List[(Nurse, NurseRole)]]] =
    val requiredNumberOfNurses = req.numberOfNurses.to
    val availableNurses = filterNurses(nurses, req.role)
    if(availableNurses.size >= requiredNumberOfNurses)
      Right(availableNurses.map(nurse => (nurse, req.role)).combinations(requiredNumberOfNurses).toList)
    else
      Left(NotEnoughNurses)

  /*
    Filters all the nurses have a specific role
  */
  def filterNurses(nurses : List[Nurse], role : NurseRole) : List[Nurse] =
    nurses.filter(nurse => nurse.roles.contains(role))

  /*
    Combines two possible combinations of the current state of combinations and the current requirement combination
    while filtering the possible combinations that contains nurses on both lists
  */
  def combineSets(listOne : List[List[(Nurse, NurseRole)]], listTwo : List[List[(Nurse, NurseRole)]]) : Result[List[List[(Nurse, NurseRole)]]] =
    val result =
      for
        listOneElement <- listOne
        listTwoElement <- listTwo
        if ( listOneElement.forall( (nurseOne, _) => !listTwoElement.exists( (nurseTwo, _) => nurseOne == nurseTwo)) )
      yield listOneElement ++ listTwoElement

    if(result.isEmpty || result.forall(subList => subList.isEmpty))
      Left(NotEnoughNurses)
    else
      Right(result)

  /*
    Combines two possible combinations of the current state of combinations for all previous periods and the current period combination
    while filtering the possible combinations that contains nurses with more shifts than allowed by the constraint
  */
  def combinePeriodSets(listOne : List[List[(NonEmptyStringST, List[(Nurse, NurseRole)])]], listTwo : List[(NonEmptyStringST, List[(Nurse, NurseRole)])], constraint: Constraint) : Result[List[List[(NonEmptyStringST, List[(Nurse, NurseRole)])]]] =
    val result =
      for
        listOneElement <- listOne
        mapOfNursesAlreadyUsed = countNumberOfNursesInPeriods(listOneElement)
        listTwoElement <- listTwo
        mapOfNursesToBeUsed = countNumberOfNursesInPeriods(List(listTwoElement))
        if ( checkConstraintByValue(mapOfNursesAlreadyUsed, mapOfNursesToBeUsed, constraint.maxShiftsPerDay.to) )
      yield listOneElement :+ listTwoElement

    if(result.isEmpty || result.forall(subList => subList.isEmpty))
      Left(NotEnoughNurses)
    else
      Right(result)

  /*
  Combines two possible combinations of the current state of combinations for all previous periods and the current period combination
  while filtering the possible combinations that contains nurses with more shifts than allowed by the constraint
*/
  def combineDaySets(listOne : List[List[RequirementInfo]], listTwo : List[List[RequirementInfo]], constraint: Constraint, basePeriodCombination : List[List[PeriodRequirementInfo]]) : Result[List[List[RequirementInfo]]] =
    val maxNumberOfWorkingDays = 7 - constraint.minRestDaysWeek.to
    val result =
      for
        listOneElement <- listOne
        mapOfNursesAlreadyUsed = countNumberOfNursesInDays(listOneElement)
        listTwoElement <- listTwo
        mapOfNursesToBeUsed = countNumberOfNursesInDays(listTwoElement)
        mapOfTotalNursesUsed = ((mapOfNursesAlreadyUsed.toSeq ++ mapOfNursesToBeUsed).groupMapReduce(_._1)(_._2)(_+_))

        if ( checkConstraintByValue(mapOfTotalNursesUsed, maxNumberOfWorkingDays) &&
          validateRestOfTheWeekPossibilities(mapOfTotalNursesUsed, basePeriodCombination, listTwoElement, maxNumberOfWorkingDays))
      yield listOneElement ++ listTwoElement

    if(result.isEmpty || result.forall(subList => subList.isEmpty))
      Left(NotEnoughNurses)
    else
      Right(result)

      /*
      Possible filters that could help in the combination performance by filtering for combinations with preferences equal or bigger than zero
      With this filters, xml test 4 and 5 passes in good time but xml 3 fails.

      val filteredResult1 = result.filter(r => r.forall(b => b.weight >= 0)) //filter for combinations with weigth bigger or equal to zero
      val filteredResult2 = filteredResult1.filter(r => r.forall(b => b.weight > 0)) //filter for combinations with weigth bigger than zero


      if ((filteredResult1.isEmpty) && (filteredResult2.isEmpty))
        Right(result)
      else
        if (filteredResult2.isEmpty)
          Right(filteredResult1)
        else
          Right(filteredResult2)
      */

  /*
    Auxiliar method that allows to create a map with all the used nurses and the number of times used
  */
  def countNumberOfNursesInPeriods(list : List[(NonEmptyStringST, List[(Nurse, NurseRole)])]) : Map[Nurse, Int] =
    list.flatMap((period, subList) => subList.map((nurse, _) => (nurse, period))).distinct.groupBy((nurse, _) => nurse).map((nurse, list) => (nurse, list.size))

  def countNumberOfNursesInDays(list : List[RequirementInfo]) : Map[Nurse, Int] =
    val t = list.map(req => (req.nurse, req.day))
      .distinct
      .groupBy((nurse, _) => nurse)
      .map((nurse, list) => (nurse, list.size))
    t

  /*
    Method to check if the current total count of nurses and the current count of nurses still obey to the constraint
    of number of shifts per day
  */
  def checkConstraintByValue(mapOne : Map[Nurse, Int], mapTwo : Map[Nurse, Int], maxValue : Int) : Boolean =
    checkConstraintByValue(((mapOne.toSeq ++ mapTwo).groupMapReduce(_._1)(_._2)(_+_)), maxValue)

  def checkConstraintByValue(mapTotal : Map[Nurse, Int], maxValue : Int) : Boolean =
    mapTotal.forall { case (nurse, count) => count <= maxValue}

  def validateRestOfTheWeekPossibilities(mapTotal : Map[Nurse, Int], periodCombination : List[List[PeriodRequirementInfo]], currentReq : List[RequirementInfo], maxValue : Int) : Boolean =
//    val currentDay = currentReq.map(_.day.to).min
//    val missingDays = 7 - currentDay
//    val possiblePeriodCombination = List.fill(2)(periodCombination).flatten
//    val newPossibleCombinations = possiblePeriodCombination.combinations(missingDays).toList
//
//    val anyPossibleComb = newPossibleCombinations.filter(newPossibleComb => isPossibleComb(mapTotal, newPossibleComb, maxValue))
//    !anyPossibleComb.isEmpty
    val excludedNurses = mapTotal.filter{case (_, nr) => nr > maxValue}.keys.toList
    if(excludedNurses.isEmpty)
      true
    else
      val currentPeriodReq = currentReq.map(req => PeriodRequirementInfo(req.period, req.nurse, req.nurseRole))
      val remainingPeriodCombination = retrieveSubListFromElem(periodCombination, currentPeriodReq)
      val remainingFilteredPeriodCombination = removeExcludedNursesFromNursesAllComb(remainingPeriodCombination, excludedNurses)

      !remainingFilteredPeriodCombination.isEmpty

//  def isPossibleComb(mapTotal : Map[Nurse, Int], remainingPossibleComb : List[List[PeriodRequirementInfo]], maxValue : Int) : Boolean =
//    val remainingNurseCountMap = remainingPossibleComb.flatMap(reqs => reqs.map(req => (req.nurse, 1)))
//      .groupBy(((nurse, _) => nurse))
//      .map((nurse, list) => (nurse, list.map(_._2).sum))
//    val mapOfTotalNursesUsed = ((mapTotal.toSeq ++ remainingNurseCountMap).groupMapReduce(_._1)(_._2)(_+_))
//    val excludedNurses = mapTotal.filter{case (_, nr) => nr > maxValue}.keys.toList
//
//    excludedNurses.isEmpty


//    val currentReqNurses = currentReq.map(_.nurse)
//    val maxCount: Int = mapTotal.filter((nurse, _) => currentReqNurses.contains(nurse)).valuesIterator.max
//    val maxPossibleUsagesLeft = maxValue - maxCount
//    val newMapTotal = (1 to maxPossibleUsagesLeft).foldLeft(mapTotal)((accum, _) => updateAvailableNurses(accum, currentReqNurses))
//    var currentDay = currentReq.map(_.day.to).min
//    var numberOfDaysLeft = Math.max(currentDay, maxValue) - Math.min(currentDay, maxValue)
//
//    if(numberOfDaysLeft > 0 && maxPossibleUsagesLeft != numberOfDaysLeft)
//      val excludedNurses = newMapTotal.filter { case (_, nr) => nr > maxValue }.keys.toList
//
//      if(!excludedNurses.isEmpty)
//        val remainingFilteredPeriodCombination = removeExcludedNursesFromNursesAllComb(periodCombination, excludedNurses)
//        val t = !remainingFilteredPeriodCombination.isEmpty
//        t
//      else
//        true
//    else
//      true
//
////    //    val newMapTotal = updateAvailableNurses(mapTotal, currentReqNurses)
////
////
////    val excludedNurses = newMapTotal.filter { case (_, nr) => nr > maxValue }.keys.toList
////    //    if(excludedNurses.isEmpty)
////    //      true
////    //    else
////    //      val currentPeriodReq = currentReq.map(req => PeriodRequirementInfo(req.period, req.nurse, req.nurseRole))
////    //      val remainingPeriodCombination = retrieveSubListFromElem(periodCombination, currentPeriodReq)
////    //      val remainingFilteredPeriodCombination = removeExcludedNursesFromNursesAllComb(remainingPeriodCombination, excludedNurses)
////    val remainingFilteredPeriodCombination = removeExcludedNursesFromNursesAllComb(periodCombination, excludedNurses)
////
////    val t = !remainingFilteredPeriodCombination.isEmpty
////    t
//

  def retrieveSubListFromElem(periodCombination : List[List[PeriodRequirementInfo]], currentReq : List[PeriodRequirementInfo]) :  List[List[PeriodRequirementInfo]] =
    val index = periodCombination.indexOf(currentReq)
    periodCombination.slice(index, periodCombination.size - 1)

  def removeExcludedNursesFromNursesAllComb(periodCombination : List[List[PeriodRequirementInfo]], excludedNurses : List[Nurse]) : List[List[PeriodRequirementInfo]] =
    periodCombination.filter(periodCombination => periodCombination.forall(req => !excludedNurses.contains(req.nurse)))

  def updateAvailableNurses(availableNurses: Map[Nurse, Int], currentUsed: List[Nurse]) : Map[Nurse, Int] =
    availableNurses.map{case (nurse, nr) => {
      if(currentUsed.contains(nurse))
        (nurse, nr + 1)
      else
        (nurse, nr)
    }}

  def checkDayCombinations(periodReqComb : List[List[PeriodRequirementInfo]], maxValue : Int) : List[List[List[PeriodRequirementInfo]]] =
    List.fill(7)(periodReqComb).flatten.combinations(7).flatMap(_.permutations).filter(reqCombWeek => isPossibleComb(reqCombWeek, maxValue)).toList

//    val availableCombinations = allPeriodReqComb.combinations(7).toList
//    availableCombinations.filter(comb => isPossibleComb(comb, maxValue))

  def isPossibleComb(possibleComb : List[List[PeriodRequirementInfo]], maxValue : Int) : Boolean =
    val possibleNurseCount = possibleComb.flatMap(reqs => reqs.map(req => (req.nurse, 1)))
      .groupBy(((nurse, _) => nurse))
      .map((nurse, list) => (nurse, list.map(_._2).sum))
    val excludedNurses = possibleNurseCount.filter{case (_, nr) => nr > maxValue}.keys.toList
    excludedNurses.isEmpty

  def convertToReqInfo(periodReqComb : List[List[List[PeriodRequirementInfo]]], nursesPreferenceWeight : Map[(DayOfWeek, NonEmptyStringST, Nurse), Int]) : Result[List[List[RequirementInfo]]] =
    if(periodReqComb.isEmpty)
      Left(NotEnoughNurses)
    else
      val daysOfWeek = WeekDay.values.map(_.value)
      val indexList = (0 to 6).toList
      val possibilities =
        periodReqComb.map(weekComb => {
          for
            index <- indexList
            dayOfWeek = daysOfWeek(index)
            periodReqWeekComb = weekComb(index)
            reqWeekComb = periodReqWeekComb.map(periodReq =>{
              val nurseWeight = getNursePreferenceWeight(periodReq.nurse, dayOfWeek, periodReq.period, nursesPreferenceWeight)
              RequirementInfo(dayOfWeek, periodReq.period, periodReq.nurse, periodReq.nurseRole, nurseWeight)
            })
          yield reqWeekComb
        })

      Right(possibilities.map(_.flatten))