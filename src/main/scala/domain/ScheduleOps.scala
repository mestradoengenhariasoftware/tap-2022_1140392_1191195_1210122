package domain

import domain.entities.{ScheduleInfo, ShiftSchedule}

trait ScheduleOps:
  def createNurseSchedule(s: ScheduleInfo): Result[ShiftSchedule]