package domain.schedule

import scala.xml.Elem
import domain.{Result, ScheduleServiceV2}
import xml.{DomainToXML, XMLtoDomain}

object ScheduleMS03 extends Schedule:

  // TODO: Create the code to implement a functional domain model for schedule creation
  //       Use the xml.XML code to handle the xml elements
  //       Refer to https://github.com/scala/scala-xml/wiki/XML-Processing for xml creation
  def create(xml: Elem): Result[Elem] =
    for
      info <- XMLtoDomain.scheduleInfo(xml)
      ss <- ScheduleServiceV2.createNurseSchedule(info)
    yield DomainToXML.toResult(ss)