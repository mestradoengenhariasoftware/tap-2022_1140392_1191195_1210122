package domain.schedule

import scala.xml.Elem
import domain.{Result, ScheduleService}
import xml.{DomainToXML, XMLtoDomain}
import domain.DomainError.*

object ScheduleMS01 extends Schedule:

  // TODO: Create the code to implement a functional domain model for schedule creation
  //       Use the xml.XML code to handle the xml elements
  //       Refer to https://github.com/scala/scala-xml/wiki/XML-Processing for xml creation
  def create(xml: Elem): Result[Elem] =
    for
      info <- XMLtoDomain.scheduleInfo(xml)
      ss <- ScheduleService.createNurseSchedule(info)
    yield DomainToXML.toResult(ss)